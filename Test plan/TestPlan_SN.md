# TEST PLAN
>Version 1.0, October 25, 2019

>Prepared  by:  
>* _Borislav Georgiev_  
>* _Nikolay Nikolov_  
>* _Vesela Slavkova_


### TABLE OF CONTENTS


1. **[INTRODUCTION](#introduction)**

2. **[OBJECTIVES AND TASKS](#objectives_and_tasks)**
    
    2.1. **[Objectives](#objectives)**

    2.2. **[Tasks](#tasks)**

3. **[SCOPE](#scope)**
    
    3.1. **[Features to be tested](#features_to_be_tested)**
    
    3.2. **[Features not to be tested](#features_not_to_be_tested)**

4. **[TESTING STRATEGY](#testing_strategy)**

    4.1. **[Testing levels](#testing_levels)**

    4.2. **[Types of testing](#types_of_testing)**

5. **[TESTING PROCESS](#testing_process)**

    5.1. **[Test Deliverables](#test_deliverables)**

    5.2. **[Responsibilities](#responsibilities)**

    5.3. **[Resources](#resources)**

    5.4. **[Estimation and Schedule](#estimation_and_schedule)**

6. **[ENVIRONMENTAL REQUIREMENTS](#environmental_requirements)**

7. **[RISKS AND ASSUMPTIONS](#risks_and_assumptions)**

8. **[CONTROL PROCEDURES](#control_procedures)**

9. **[REVISION HISTORY](#revision_history)**

### 1. <a name="introduction"></a>INTRODUCTION

[Social Network](https://bla-bla.com/) (**SN**) is a web-based application used by variable users. **SN** offers opportunities to:
  - Connect with people;
  - Create, comment and like posts;
  - Get a feed of the newest/most relevant posts of your connections;
  - User Profile Management;
  - Secure authentication and authorization;
  - Search Engine.

#### Project Details

| | |
|-|-|
| **Project Name** | QA Final Project 2019 |
| **Product Name** | Social Network (SN) |
| **Product Release Version** | 1.0 |
| **Product URL** | www.bla-bla.com |

### 2. <a name="objectives_and_tasks"></a>OBJECTIVES AND TASKS

#### 2.1 <a name="objectives"></a>Objectives
Describe, at a high level:
- the scope for testing
- approach for the testing activities
- resources for testing
- schedule of the testing activities

This document is a “Master Test Plan for Social Network v1” and Service Level Agreement documents and it aims at:

- outlining the overall testing effort needed for delivering fully operational and stable in-scope system features and their respective functionalities;
- coordinating all necessary testing and control activities;
- guaranteeing uninterrupted test workflow and adequate and flexible approach to eventual unexpected interruption / incident;
- determining entry and exit criteria, time constraints, role responsibilities, means of communication, problem reporting, risk assessment and control activities;
- facilitating communication between team members;
- setting uniform understanding between team members of what is needed for successful completion of the testing tasks.

#### 2.2. <a name="tasks"></a>Tasks

The main tasks that will be completed with the successful execution of the test plan are:

- Detailed analysis of the in-scope features and respective functionalities;
- Selection of the appropriate testing levels and types of tests;
- Authoring of low level test cases;
- Manual test case execution;
- Automating test cases;
- Preparation of detailed test and bug reports.

### 3. <a name="scope"></a>SCOPE

The testing efforts will be focused on the latest versions of the following functionalities:

- access functionality;
- search functionality;
- functionality for sorting posts;
- user profile management features;
- user interactions with other users on authorization levels “regular user” and "administrator";
- functionality for creation, commenting and liking a post.

#### 3.1.  <a name="features_to_be_tested"></a>Features to be tested

All SN features that fall into the testing scope of this document will be reviewed and verified from a regular user’s point of view:

- the registration and login forms;

- access permissions on authorization levels “regular user” and "administrator";

- preferences functionality of the User Profile Management features:
    * Change name;
    * Upload, change and remove a profile picture;
    * Set visibility of profile (public or private);
    * Change password;
    * Change email;
    * Add other information /?/

- interactions between users functionality:
    * Send and approve connection requests;
    * Disconnection doesn't need approval;
    * User status depends on current connection to other user;
    * Create, edit and delete a comment to other user`s post;
    * Like/Unlike other user`s post;

- Like counter, showed along the post content;

- creation of post features:
    * Create, edit and delete a post as user;
    * Set visibility of post (public or private);
    * Upload content to the post (picture, music, video, location etc.);

- functionality of users` personalized post feed, formed from their connection’s posts by an appropriate filter;

- functionality of permitted activities on authorization level "administrator":
    * Edit and delete profiles;
    * Edit and delete posts;
    * Edit and delete comments.

#### 3.2.  <a name="features_not_to_be_tested"></a>Features not to be tested

#### 3.2.1. Related to section “Features to be tested”

- Secure level of password;
- Summary about user activities - posts, comments, likes, connect and disconnect requests;
- Format text of the post features;
- Share a post/comment functionality;
- Counter for posts, comments, comments` likes.

#### 3.2.2. Not Related to section “Features to be tested”

- Chat features;
- Personal messages feature;
- Notifications feature.

### 4. <a name="testing_strategy"></a>TESTING STRATEGY

#### 4.1. <a name="testing_levels">Testing levels

The testing effort of the QA team will be focused on System level where end-to-end user scenarios will be tested via functional and regression tests.

#### 4.2. <a name="types_of_testing">Types of testing

- **Functional Testing** - testing conducted to evaluate the compliance of a component or system with functional requirements. (ISTQB).

- **Security Testing** - type of testing which makes sure that unauthorized access is prevented and data is adequately protected.

- **Regression Testing** - testing of a previously tested component or system following modification to ensure that defects have not been introduced or have been uncovered in unchanged areas of the software, as a result of the changes made.

- **Usability Testing** - testing to evaluate the degree to which the system can be used by specified users with effectiveness, efficiency and satisfaction in a specified context of use.
 
| Environment | Types of tests | Responsible team |
|-------------|----------------|------------------|
| DEV environment | - Unit Tests<br>- Integration Tests | Development Team |
| | - Regression tests (smoke tests) | QA Team |
| UAT environment | - Functional Testing (end-to-end scenarios) | QA team for end-to-end tests |
| | - Security Testing | Security Awareness Team |
| | - Acceptance Testing | Product owner`s for Acceptance Testing  |
| Live environment | - Functional Testing (end-to-end scenarios) | QA team for end-to-end tests |
| | - Usability Testing | Product owner`s for Usability Testing |

### 5. <a name="testing_process"></a>TESTING PROCESS

#### Entry criteria

 - 100% of unit and integration tests in DEV environment pass;
 - In-scope features are deployed in UAT environment and code freeze stage is met;
 - High level test cases are authored as per approved template;
 - Automation server is deployed and ready for use;
 - All needed hardware and software is properly set up and provided by the hardware and dev-ops teams;
 - Quality testing data is set up and available for use by the dev-ops team.

#### Exit criteria

 - 100% of Prio1 tests pass;
 - 90% of Prio2 tests pass;
 - 100% of regression tests pass;
 - 100% of bugs with Severity “Blocking” and “Critical” are fixed;
 - Time has ran out.

#### Test Cases

#### Criteria for test case prioritization

Criteria that should be taken into consideration when setting test case priority are:

 - Product owner’s  prioritization of requirements outlined in the Service Level Agreement document;
 - In-scope feature areas with the highest number of previously logged bugs with high and medium priority;
 - In-scope feature areas of similar products which proved to be bottlenecks in the past;
 - In-scope feature functionalities which are considered most complex or with critical infrastructure;
 - In-scope feature functionalities with most visible to the client`s potential failures.


#### Priority Levels

 - **Priority – 1 (Prio1)**: Allocated to all tests that must be executed in any case.
 - **Priority – 2 (Prio2)**: Allocated to the tests which can be executed, only when time permits.
 - **Priority – 3 (Prio3)**: Allocated to the tests, which even if not executed, will not cause big disruption.

#### Bug severity levels

Severity is the degree of impact that a defect has on the development or operation of a component or system. (ISTQB). The severity will be set by the person who finds and logs the bug. 

There are four severity levels which will be used by the team:

- _S1: Blocking_ - a defect that completely hampers or blocks testing of the product / feature is a critical defect. It does not have a workaround.
- _S2: Major_ - a defect which needs immediate attention as it seriously hampers the feature usage / testing, however it has a workaround. The workaround is not obvious.
- _S3: Minor_ - the defect affects minor functionality or non-critical data. It has an easy workaround.
- _S4: Trivial_ - the defect does not affect functionality or data. It does not impact productivity or efficiency of the features.

<p style="text-align: center;">
	<img src="https://content.screencast.com/users/kocteh/folders/Default/media/49d26c71-4104-4f68-8952-0618eb930000/bug_severity.png" alt="Bug Severity Levels"/>
</p>

As the severity will be used in the bug triage process and prioritization, it is imperative that there is a common understanding between the QA team members of what the term implies. When setting the bug priority, it is important that the below key points are taken into consideration:

- Spend sufficient time obtaining clear understanding of how the defect will affect the end user;
- Always assign the severity level based on the issue type as this will affect its priority.

####  Bug prioritization and triaging

Priority is the level of (business) importance assigned to an item, e.g., defect. (ISTQB). The bug priority reveals the importance and the urgency of the defect.

Bug prioritization will be performed during the triaging sessions. The bug priority levels that will be used when reviewing bugs are:

 - _P1: High_ - the bug needs immediate attention, it should be fixed right away and the features cannot be released with this bug.
 - _P2: Medium_ - the bug needs to be fixed as early as possible after all critical bugs have been fixed.
 - _P3: Low_ - the bug is with minor importance and its correction will be left for future releases, unless there are no bugs left with P1 and P2.

Bug triaging will be performed at the beginning of every iteration by the defect management committee which includes the Project Manager, Team Leader and the two senior QAs. During triaging the defect management committee will determine the priority of every bug and will select the bugs that will be included in the iteration based on several considerations:

 - Bug severity;
 - Issue type;
 - Impact;
 - Prioritization set by in the client in the Service Level Agreement document.

Bug priority might change due to time constraints and changes in the importance of the defect. In such cases the defect management committee will re-prioritize the bug accordingly.

#### 5.1. <a name="test_deliverables">Test Deliverables

##### 5.1.1. Test Plan

Test plan will be logged in GitLab Project  **FP_TA_SNAPI** - https://gitlab.com/nikolainnikolov/fp_ta_snapi

##### 5.1.2. List with test cases

Full list with high level test cases will be logged in GitLab Project  **FP_TA_SNAPI** - https://gitlab.com/nikolainnikolov/fp_ta_snapi (Test Cases folder)

##### 5.1.3. Bug reports

All bug reports will be logged in GitLab Project **FP_TA_SNAPI** - https://gitlab.com/nikolainnikolov/fp_ta_snapi/issues 

The bug reports should contain at least the following information:
 - Title;
 - Description;
 - Steps to reproduce;
 - Expected result;
 - Actual result;
 - Environment;
 - Severity;
 - Additional information (if applicable) - screenshots, images, videos, any other additional information which helps scope the issue.

##### 5.1.4. Test reports

All test reports will be logged in GitLab Project **FP_TA_SNAPI** - https://gitlab.com/nikolainnikolov/fp_ta_snapi (Test Reports Folder)

#### 5.2. <a name="responsibilities"></a>Responsibilities

| Type of activity | Responsibility of |
|------------------|-------------------|
| Planning and Managing | - Nikolay Nikolov<br> - Borislav Georgiev<br> - Vesela Slavkova|
| Designing High level Test Cases | - Nikolay Nikolov<br> - Borislav Georgiev<br> - Vesela Slavkova|
| Regression test authoring and execution<br>Authoring of in-scope feature test cases | - Nikolay Nikolov<br> - Borislav Georgiev<br> - Vesela Slavkova|
| Test case automation and automated execution |**Selenium**:<br> - Nikolay Nikolov<br> - Borislav Georgiev<br> - Vesela Slavkova|
| Test Case Manual Execution | - Nikolay Nikolov<br> - Borislav Georgiev<br> - Vesela Slavkova|
| Bug and test case summary reports | - Nikolay Nikolov<br> - Borislav Georgiev<br> - Vesela Slavkova|

#### 5.3. <a name="resources"></a>Resources

For the performance of the testing tasks, the following automated tools will be used:

- Organization and distribution of the tasks - https://trello.com/b/GrZuiEAm/qa-team-project-social-network
- Test case tracking and reporting system - https://gitlab.com/nikolainnikolov/fp_ta_snapi
- Automation tool used to simulate REST calls - 
- Middle tier tests automation - Postman
- Functional tests covering the UI automation - Selenium and Sikuli
- Bug tracking and reporting system -  https://gitlab.com/nikolainnikolov/fp_ta_snapi/issues
- Main communication channels - Forum Group “Apha-QA-May-2019” - https://forum.telerikacademy.com/c/Alpha-QA-May-2019

#### 5.4. <a name="estimation_and_schedule"></a>Estimation and Schedule

Time for overall testing effort is set to 2 weeks (10 working days).. Time has been allocated within the project plan for the following testing activities:

|Order of execution| Test Activity | Degree of complexity<br>high 1-5 low |Time consuming days/person|
|------|------------------|------|------------------|
|1|Test Plan|3|3 days|
|2|High level test cases|2|5 days|
|3|REST API Testing|1|10 days|
|4|UI Testing  |1|10 days|
|5|Bug Summary Report|4|1 day|
|6|Test Summary Report|4|1 day|
|7|Preparing Presentation  of the Project|5|1 day|

### 6. <a name="environmental_requirements"></a>ENVIRONMENTAL REQUIREMENTS

#### 6.1. <a name="hardware_requirements">Hardware requirements:

- Desktop and Laptop Machines on Windows 10;
- .............

#### 6.2. <a name="software_requirements">Software requirements:

- Latest versions of Google Chrome, Mozilla Firefox, Edge  on Windows 10;
- Latest version of Google Chrome on latest version of Android;

### 7. <a name="risks_and_assumptions"></a>RISKS AND ASSUMPTIONS

 - code freeze stage was not reached on time  / development of in-scope features is not finished on time;
 - test effort timelines were unrealistically set;
 - unexpected unavailability of team members both from the developers and from the QAs;
 - lack of availability of required hardware, software, data or tools;
 - late delivery of the software, hardware or tools;
 - changes to the original requirements or designs;
 - changes in the prioritization of requirements set by the client in the Service Level Agreement document;
 - unexpected complexities in testing of the in-scope features.

### 8. <a name="control_procedures"></a>CONTROL PROCEDURES

The current test plan has been created to improve the productivity of the testing effort. Consequently, it is essential that correct procedures are set in place to ensure that deviations from the test plan can be addressed in a timely manner and in the best possible way. Control helps us deal with any unexpected situation that might impede the testing activities needed for the delivery of a quality in-scope features. 

All control activities will be performed by the Project Manager. The Project Manager will ensure that:

 - all team members involved in the quality assurance process understand correctly and agree with the activities described in this document at any given step;
 - all testing activities follow the rules set in the document;
 - all testing activities are on track and performed on time;
 - all misunderstandings are cleared out in a timely manner;
 - all deviations from the processes described in this document are found on time and are addressed in a timely manner;
 - there are set procedures which help the team answer any unexpected situation which might impede or delay the testing activities;
 - all team members are familiar with the ways an encountered incident can be reported;
 - all control activities are conducted in a discrete way which does not interrupt in any way the normal workflow and day-to-day work of the team members.

#### Reporting incidents:

Every Monday during the team brief meetings on the progress of the testing work, 5 minutes will be used for incident reporting and incident resolution progress reporting so that the whole team is aware of any obstacles that were encountered and the impact that they (might) have on the progress of the testing effort. If, for any reason, the incident reports cannot be performed during these meetings, any team member may send an email to the Project Manager and Team Leader explaining as much as possible the circumstances of the incident (when it occurred, what is the potential impact, what part of the testing process is affected, any piece of information that might assist in finding the root cause).

#### Addressing incidents:

If the incident affects any main point outlined in this document (e.g. estimated timelines and resources, outlined environmental needs, entry and exit criteria) and its resolution cannot be immediately reached, the Project Manager will ensure that the agreed changes are reflected in the correct document section and they are recorded in the document revision history.

In case of absence of the Project Manager, the Team Leader will carry on with the PM’s control responsibilities.

### 9. <a name="revision_history"></a>REVISION HISTORY


| # | Type of Change | Name of contributor(s) | Date of Creation | Approved By | Date of Approval |
|---|----------------|------------------------|------------------|-------------|------------------|
| 1. | Initial Draft | | |  | |
