﻿# TEST PLAN
>Version 2.0, October 30, 2019

>Final review, December 01,2019

>Prepared  by:
* _Vesela Slavkova_
* _Borislav Georgiev_
* _Nikolay Nikolov_



### TABLE OF CONTENTS


1. **[INTRODUCTION](#introduction)**

2. **[OBJECTIVES AND TASKS](#objectives_and_tasks)**

    2.1. **[Objectives](#objectives)**

    2.2.**[Tasks](#tasks)**
3. **[SCOPE](#scope)**

    3.1. **[Features to be tested](#features_to_be_tested)**

    3.2. **[Features not to be tested](#features_not_to_be_tested)**
4. **[TESTING STRATEGY](#testing_strategy)**

    4.1.**[Testing levels](#testing_levels)**

    4.2.**[Types of testing](#types_of_testing)**
5. **[TESTING PROCESS](#testing_process)**

    5.1.**[Test Deliverables](#test_deliverables)**

    5.2.**[Responsibilities](#responsibilities)**

    5.3.**[Resourses](#resourses)**

    5.4.**[Estimation and Schedule](#estimation_and_schedule)**

6. **[ENVIRONMENTAL REQUIREMENTS](#environmental_requirements)**
7. **[RISKS AND ASSUMPTIONS](#risks_and_assumptions)**
8. **[CONTROL PROCEDURES](#control_procedures)**
9. **[FINAL REVIEW](#final_review)**


### 1. <a name="introduction"></a>INTRODUCTION

[Social Alien Network](http://localhost:8080/) (**SAN**) is a web-based application used by variable users. **SAN** offers opportunities for:
- Connect with people
- Create, comment and like posts
- Get a feed of the newest/most relevant posts of your connections;
- User Profile Management;
- Secure authentication and authorization;
- Search Engine.

#### Project Details

| | |
|-|-|
| **Project Name** | QA Final Project 2019 |
| **Product Name** | Social Alien Network(SAN) |
| **Product Release Version** | 1.0 |
| **Product URL** |  |

### 2. <a name="objectives_and_tasks"></a>OBJECTIVES AND TASKS
#### 2.1. <a name="objectives"></a>Objectives

This document is a “Master Test Plan for Social Alien Network v2” and Service Level Agreement documents and it aims at:

- outlining the overall testing effort needed for delivering fully operational and stable in-scope system features and their respective functionalities;
- coordinating all necessary testing and control activities;
- guaranteeing uninterrupted test workflow and adequate and flexible approach to eventual unexpected interruption / incident;
- determining entry and exit criteria, time constraints, responsibilities, means of communication, problem reporting, risk assessment and control activities;
- facilitating communication between team members;
- setting uniform understanding between team members of what is needed for successful completion of the testing tasks.

#### 2.2. <a name="tasks"></a>Tasks

The main tasks that will be completed with the successful execution of the test plan are:

- Detailed analysis of the in-scope features and respective functionalities;
- Selection of the appropriate testing levels and types of tests;
- Authoring of high level tests;
- Design and Execution tests based on exploratory test technique;
- Automating test cases;
- Manual test case execution;
- Creation of detailed test and bug reports that include the result of the performed tests both manually and automated.

### 3. <a name="scope"></a>SCOPE

The testing efforts will be focused on the latest versions of the following functionalities:

- access functionality
- search functionality
- user profile management features
- user interactions with other users on authorization levels “regular user” and "administrator"
- functionality for creation, comment and like post
- functionality for order posts
#### 3.1.  <a name="features_to_be_tested"></a>Features to be tested

All SA features that fall into the testing scope of this document will be reviewed and verified from a regular user’s point of view:

- registration and login forms;
- access permissions on authorization levels “regular user” and "administrator"
- preferences functionality of the User Profile Management features:
    * Change name
    * Upload, change and remove a profile picture
    * Set visibility of profile (public or private)
    * Change email
    * Change password
    * Add personal information - date and city of birth, city and planet of residence
- interactions between users functionality:
    * Send and approve connection requests;
    * Disconnection doesn't need approval;
    * User status depends on current connection to other user
    * Create, edit and delete a comment to other user`s post
    * Like/Unlike other user`s post
- Like counter, showed along the post content;  
- creation of post features:
    * Create, edit and delete a post as user
    * Set visibility of post(public or private)
    * Upload a content to the post, different ftom text
- functionality of users` personalized post feed, formed from their connection’s posts by an appropriate filter

- functionality of permitted activities on authorization levels "administrator":
    * Edit and Delete profiles
    * Edit and Delete posts
    * Edit and Delete comments

#### 3.2.  <a name="features_not_to_be_tested"></a>Features not to be tested

- Secure level of password;
- Summary about user activities - posts, comments, likes, connect and disconect requests;
- Format text of the post features
- Share a post/comment functionality;
- Personal messages feature;
- Notifications feature.

### 4. <a name="testing_strategy"></a>TESTING STRATEGY

#### 4.1. Testing levels

The testing effort of the QA team will be focused on **System level** where end-to-end user scenarios will be tested via smoke and regression tests **Acceptance level** where the system will be tested for its performance and compliance with customer requirements for intuitive, simple and user-friendly UI.

#### 4.2. Types of testing

**Functional Testing** - testing conducted to evaluate the compliance of a component or system with functional requirements.
- **Smoke Tests** - testing verify the major functionality on a high level.Smoke Tests should be automated and take less than 2-3 hours.  
The objective is to determine if further testing is possible.  These test cases should emphasize breadth more than depth.  All components should be touched, and every major feature should be tested briefly by the Smoke Test. If any test case on this level fails, the build is returned to developers un-tested.
- **Regression Testing** - testing of a previously tested component or system following modification to ensure that defects have not been introduced or have been uncovered in unchanged areas of the software, as a result of the changes made.
Every bug that was “Open” during the previous build, but marked as “Fixed, Needs Re-Testing” for the current build under test, will need to be regressed, or re-tested.  Once the smoke test is completed, all resolved bugs need to be regressed.  It should take between 5 minutes to 1 hour to regress most bugs.

**Exploratory testing** -  QA Team will be perform exploratory testing oF **SAN** under test, to uncover the scenarios which are not covered as part of test case design and automated test suites.

**Nonfunctional Testing**
- **Security Testing** - type of testing which makes sure that unauthorized access is prevented and data is adequately protected.

- **Usability Testing** - testing to evaluate the degree to which the system can be used by specified users with effectiveness, efficiency and satisfaction in a specified context of use.
These cases will be run once, not repeated as are the test cases in previous levels.

### 5. <a name="testing_process"></a>TESTING PROCESS

#### Define Test activities

- Understanding Requirements:
    * Requirement specifications will be sent by client.
    * Understanding of requirements will be done by QA
- Preparing Test Cases: QA team will be preparing test cases based on the exploratory testing. This will cover all scenarios for requirements
- Creating Test Data: Test data will be created by respective QA on client's developments/test site based on scenarios and Test cases.
- Executing Test Cases:
    * Test cases will be executed by respective QA  based on designed scenarios, test cases and Test data.
    * Test result (Actual Result, Pass/Fail) will updated in test case document Defect Logging and Reporting. QA will be logging the defect/bugs in a document, found during execution of test cases. After this, QA will inform respective developer about the defect/bugs.
- Retesting and Regression Testing: Retesting for fixed bugs will be done by respective QA once it is resolved by respective developer and bug/defect status will be updated accordingly.
- Delivery: Once all bugs/defect reported after complete testing is fixed and no other bugs are found, report will be provided to client.

#### Define Entry criteria

 - 100% of unit and integration tests in DEV environment pass;
 - In-scope features are deployed in UAT environment and code freeze stage is met;
 - High level test cases are authored as per approved template;
 - Automation server is deployed and ready for use;
 - All needed hardware and software is properly set up and provided by the hardware and dev-ops teams;
 - Quality testing data is set up and available for use by the dev-ops team.

#### Define Exit criteria

- 100% of Prio1 tests pass;
- 90% of Prio2 tests pass;
- 100% of regression tests pass;
- 100% of bugs with Severity “Blocking” and “Major” are fixed;
- Time has ran out.

#### Define Criteria for test case prioritization

Criteria that should be taken into consideration when setting test case priority are:

- Product owner’s prioritization of requirements outlined in the Service Level Agreement document;
- In-scope feature areas with the highest number of previously logged bugs with high and medium priority;
- In-scope feature functionalities which are considered most complex or with critical infrastructure;
- In-scope feature functionalities with most visible to the client`s potential failures.

#### Priority Levels

- **Priority – 1 (Prio1)**: Allocated to all tests that must be executed in any case.
- **Priority – 2 (Prio2)**: Allocated to the tests which can be executed, only when time permits.
- **Priority – 3 (Prio3)**: Allocated to the tests, which even if not executed, will not cause big disruption.

#### Bug severity levels

Severity is the degree of impact that a defect has on the development or operation of a component or system. The severity will be set by the person who finds and logs the bug.

There are four severity levels which will be used by the team:

- _S1: Blocking_ - a defect that completely hampers or blocks testing of the product / feature is a critical defect. It does not have a workaround.
- _S2: Major_ - a defect which needs immediate attention as it seriously hampers the feature usage / testing, however it has a workaround. The workaround is not obvious.
- _S3: Minor_ - the defect affects minor functionality or non-critical data. It has an easy workaround.
- _S4: Trivial_ - the defect does not affect functionality or data. It does not impact productivity or efficiency of the features.

As the severity will be used in the bug triage process and prioritization, it is imperative that there is a common understanding between the QA team members of what the term implies. When setting the bug priority, it is important that the below key points are taken into consideration:

- Spend sufficient time obtaining clear understanding of how the defect will affect the end user;
- Always assign the severity level based on the issue type as this will affect its priority.

####  Bug prioritization and triaging

Priority is the level of (business) importance and the urgency of the defect.
Bug prioritization will be performed during the triaging sessions and QA team will select the bugs that will be included in the iteration based on several considerations:

- Bug severity;
- Issue type;
- Prioritization set by in the client in the Service Level Agreement document.

Bug priority might change due to time constraints and changes in the importance of the defect. In such cases the defect management committee will re-prioritize the bug accordingly.

#### 5.1. Test Deliverables

##### 5.1.1. Test Plan

Test plan will be logged in GitLab Project  **FP_TA_SNAPI** - https://gitlab.com/nikolainnikolov/fp_ta_snapi

##### 5.1.2. List with test cases

Full list with high level test cases will be logged in GitLab Project  **FP_TA_SNAPI** - https://gitlab.com/nikolainnikolov/fp_ta_snapi (Test Cases folder)

##### 5.1.3. Bug reports

All bug reports will be logged in GitLab Project **FP_TA_SNAPI** - https://gitlab.com/nikolainnikolov/fp_ta_snapi/issues 

The bug reports should contain at least the following information:

| ID | Description | Steps to reproduce | Expected result | Actual result | Environment | Severity | Additional information<br>screenshots, images, videos |
|---------|---------|------------|------------|------------|------------|-----------|------------|

##### 5.1.4. Test reports

All test reports will be logged in GitLab Project **FP_TA_SNAPI** - https://gitlab.com/nikolainnikolov/fp_ta_snapi (Test Reports Folder)

The following metrics should be gathered during the testing effort:
- Total Planned Test Cases
- Total % of Executed/Not Executed Test Cases
- Total % of Executed by Priority
- Total Automated Test Cases
- Total % of Passed/Failed Test Cases by Priority
- Total Manually executed Test Cases
- Total % of Passed/Failed Test Cases by Priority
- Total Testing Effort in time for Exploratory testing by Functionality 

#### 5.2. <a name="responsibilities"></a>Responsibilities

| Type of activity | Responsibility of |
|------------------|-------------------|
| Planning and Managing | - Nikolay Nikolov, Borislav Georgiev, Vesela Slavkova|
| Designing High level Test Cases<br>Authoring of in-scope feature test cases |  - Nikolay Nikolov, Borislav Georgiev, Vesela Slavkova|
| Test case automation and automated execution |**Selenium** - Nikolay Nikolov, Borislav Georgiev, Vesela Slavkova|
| Regression test authoring and execution |  - Nikolay Nikolov, Borislav Georgiev, Vesela Slavkova|
| Test Case Manual Execution | - Nikolay Nikolov, Borislav Georgiev, Vesela Slavkova|
| Bug and test case summary reports |  - Nikolay Nikolov, Borislav Georgiev, Vesela Slavkova|

#### 5.3. <a name="resourses"></a>Resourses

For the performance of testing tasks they will be used automated tools as following:

- Organization and distribution of the tasks - https://trello.com/b/J2ecUCIW/bnv-qa-team
- Test case tracking and reporting system - https://gitlab.com/nikolainnikolov/fp_ta_snapi
- Middle tier tests automation - Postman
- Functional tests covering the UI automation - Selenium
- Bug tracking and reporting system -  https://gitlab.com/nikolainnikolov/fp_ta_snapi/issues
- Main communication channels - Forum Group “Apha-QA-May-2019” - https://forum.telerikacademy.com/c/Alpha-QA-May-2019

#### 5.4. <a name="estimation and schedule"></a>Estimation and Schedule

Time for overall testing effort is set to 2 weeks (10 working days).. Time has been allocated within the project plan for the following testing activities:

|Order of execution| Test Activity | Effort days/person|Date / Milestone|
|------|------------------|------|------------------|
|1|Test Plan|3 days|01.11.2019|
|2|High level test cases|5 days|05.11.2019|
|3|REST API Testing|10 days|15.11.2019|
|4|UI Testing  |10 days|25.11.2019|
|5|Bug Summary Report|2 days|27.11.2019|
|6|Test Summary Report|2 days|28.11.2019|
|7|Preparing Presentation of the Project|2 days|1.12.2019|

### 6. <a name="environmental_requirements"></a>ENVIRONMENTAL REQUIREMENTS

#### 6.1. Hardware requirements:
- Desktop and Laptop Machines;

#### 6.2. Software requirements:
- Latest versions of Google Chrome, Mozilla Firefox on Windows 10;

### 7. <a name="risks_and_assumptions"></a>RISKS AND ASSUMPTIONS

 - code freeze stage was not reached on time  / development of in-scope features is not finished on time;
 - test effort timelines were unrealistically set;
 - unexpected unavailability of team members both from the developers and from the QAs;
 - lack of availability of required hardware, software, data or tools;
 - late delivery of the software, hardware or tools;
 - changes to the original requirements or designs;
 - changes in the prioritization of requirements set by the client in the Service Level Agreement document;
 - unexpected complexities in testing of the in-scope features.

### 8. <a name="control_procedures"></a>CONTROL PROCEDURES

The current test plan has been created to improve the productivity of the testing effort. Consequently, it is essential that correct procedures are set in place to ensure that deviations from the test plan can be addressed in a timely manner and in the best possible way. Control helps us deal with any unexpected situation that might impede the testing activities needed for the delivery of a quality in-scope features. 

All control activities will be performed by the Project Manager. The Project Manager will ensure that:

 - all team members involved in the quality assurance process understand correctly and agree with the activities described in this document at any given step;
 - all testing activities follow the rules set in the document;
 - all testing activities are on track and performed on time;
 - all misunderstandings are cleared out in a timely manner;
 - all deviations from the processes described in this document are found on time and are addressed in a timely manner;
 - there are set procedures which help the team answer any unexpected situation which might impede or delay the testing activities;
 - all team members are familiar with the ways an encountered incident can be reported;
 - all control activities are conducted in a discrete way which does not interrupt in any way the normal workflow and day-to-day work of the team members.

 ### 9. <a name="final_review"></a>FINAL REVIEW

 QA team strictly follows the rules set in the document in their test activities. 
 During the Test process have occured circumstances, defined above as *RISKS AND ASSUMPTIONS*. They limit QA Team efforts to test some important in-scope system features and their respective functionalities.
 
 To be specific:
    The code coverage below 86% and 22% of them are flaky tests
    The code freeze was as of 18th of November 2019 
 - development of REST API Controllers is not finished - only User Rest Controller works properly
 Middle Tier Tests were executed in a very limited way.
 - development of in-scope features is not finished on time - Not performed Security testing and Performance testing

