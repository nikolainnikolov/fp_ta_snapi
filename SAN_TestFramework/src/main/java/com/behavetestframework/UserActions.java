package com.behavetestframework;

import com.behavetestframework.Utils;
import com.google.common.base.Verify;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.annotation.CheckReturnValue;

public class UserActions<n> {
    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser() {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }


    public void logIn() {
        clickElement("logInLink");
        Utils.LOG.info("Login link is clicked");
    }

    public void signInBase() {
        try {
            typeValueInField(Utils.getConfigPropertyByKey("username"), "userNameField");
            typeValueInField(Utils.getConfigPropertyByKey("password"), "passwordField");
            clickElement("logInButton");
        } catch (Exception e) {
            Utils.LOG.info("Already logged in");
        }
    }

    public void signInTest() {
        try {
            typeValueInField(Utils.getConfigPropertyByKey("usernameTest"), "userNameField");
            typeValueInField(Utils.getConfigPropertyByKey("passwordTest"), "passwordField");
            clickElement("logInButton");
        } catch (Exception e) {
            Utils.LOG.info("Already logged in");
        }
    }

    public void signInBobi() {
        try {
            typeValueInField(Utils.getConfigPropertyByKey("username1"), "userNameField");
            typeValueInField(Utils.getConfigPropertyByKey("password1"), "passwordField");
            clickElement("logInButton");
        } catch (Exception e) {
            Utils.LOG.info("Already logged in");
        }
    }

    public void signIn(String username, String password) {
        try {
            typeValueInField(username, "userNameField");
            typeValueInField(password, "passwordField");
            clickElement("logInButton");
        } catch (Exception e) {
            Utils.LOG.info("Already logged in");
        }
    }

    public void signInAdmin() {
        try {
            typeValueInField(Utils.getConfigPropertyByKey("usernameAdmin"), "userNameField");
            typeValueInField(Utils.getConfigPropertyByKey("passwordAdmin"), "passwordField");
            clickElement("logInButton");
        } catch (Exception e) {
            Utils.LOG.info("Logged in as Administrator");
        }
    }

    public void returnToHomePage() {
        waitForElementVisible("Home",3);
        Actions action = new Actions(driver);
        Utils.LOG.info("Navigate to Homepage");
        action.moveToElement(driver.findElement(By.xpath(Utils.getUIMappingByKey("Home"))))
                .click()
                .build()
                .perform();
    }

    public void logOut() {
        clickElement("logOutLink");
        Utils.LOG.info("User has logged out");
        driver.navigate().refresh();
    }

    public void clickElement(String key) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        Utils.LOG.info("Click on element " + key);
        element.click();
    }

    public void typeValueInField(String value, String field) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        clearField(field);
        Utils.LOG.info("Type "+ value + " in the " +field);
        element.sendKeys(value);
    }

    public void typeValueInFieldAndPressKey(String value, String field, Keys key) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        clearField(field);
        Utils.LOG.info("Type "+ value + " in the " + field + "and press " + key);
        element.sendKeys(value);
        element.sendKeys(key);
    }
    public void clearField(String field) {
        Utils.LOG.info("Clear " + field);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(Keys.CONTROL + "a");
        element.sendKeys(Keys.BACK_SPACE);
    }

    public void typeRandomStringInField(String number, String field) {
        int num = Integer.parseInt(number);
        String text = getRandomString(num);
        clearField(field);
        Utils.LOG.info("Type random value with " + number + " symbols in the " + field);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(text);
    }

    public String getRandomString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());
            // add symbols one by one at end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }
    public void uploadPicture(String field) {
        Utils.LOG.info("User uploaded a picture.");
        WebElement uploadElement= driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        uploadElement.sendKeys(Utils.getConfigPropertyByKey("ProfilePicture"));
        driver.findElement(By.name("send")).click();
    }

//    ### WAIT ###

    public void waitForElementVisible(String locator, int seconds) {
        Utils.LOG.info("Wait for visibility of " + locator + " up to " + seconds + " seconds");
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
    }

//    ### VERIFY ###

    public boolean verifyElementVisible(String locator) {
        try {
            driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
            Utils.LOG.info("Element visible: " + locator);
            return true;
        } catch (NoSuchElementException e) {
            Utils.LOG.info("Element NOT visible: " + locator);
            return false;
        }
    }

    public boolean verifyTextIsEqual(String locator, String locator2) {
        driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        driver.findElement(By.xpath(Utils.getUIMappingByKey(locator2)));
        WebDriverWait wait= new WebDriverWait(driver,3);
        WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        String textOfWebElement = webElement.getText();
        WebElement  webElement2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator2))));
        String textOfWebElement2 = webElement2.getText();
        try {
            Assert.assertEquals(textOfWebElement, textOfWebElement2);
            Utils.LOG.info("Texts are the same: " + textOfWebElement + ", " + textOfWebElement2);
            return true;
        } catch (NoSuchElementException e) {
            Utils.LOG.info("Texts are NOT the same: " + textOfWebElement + ", " + textOfWebElement2);
            return false;
        }
    }

    public boolean verifyTextIsEqualToLocator(String text, String locator) {
        driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        WebDriverWait wait= new WebDriverWait(driver,3);
        WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        String textOfWebElement = webElement.getText();
        try {
            Assert.assertEquals(textOfWebElement, text);
            Utils.LOG.info("Texts are the same: Locator = " + textOfWebElement + ", Text = " + text);
            return true;
        } catch (NoSuchElementException e) {
            Utils.LOG.info("Texts are NOT the same: Locator = " + textOfWebElement + ", Text = " + text);
            return false;
        }
    }

//    ### ASSERT ###

    public void assertElementPresent(String locator) {
        Utils.LOG.info("Element exists: " + locator);
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementNotPresent(String locator) {
        Utils.LOG.info("Element does NOT exist: " + locator);
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

    public void assertTextEquals(String expectedText, String locator) {
        WebDriverWait wait= new WebDriverWait(driver,4);
        WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        String textOfWebElement = webElement.getText();
        Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
        Assert.assertEquals(expectedText, textOfWebElement);
    }

    public void assertTextNotEquals(String expectedText, String locator) {
        WebDriverWait wait = new WebDriverWait(driver,4);
        WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        String textOfWebElement = webElement.getText();
        Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
        Assert.assertNotEquals(expectedText, textOfWebElement);
    }

    public void assertTextPresent(String expectedText, String locator) {
        Utils.LOG.info("Expected text exists: " + expectedText);
        WebDriverWait wait= new WebDriverWait(driver,4);
        WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        Assert.assertNotNull(driver.findElement(By.name(Utils.getConfigPropertyByKey(expectedText))));
    }
}
