package TestCases.UserInteractions;

import TestCases.BaseTest;
import com.behavetestframework.UserActions;
import com.behavetestframework.Utils;
import org.junit.*;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class User_Interactions extends BaseTest {

    private String user1 = "gosho";
    private String password1 = "goshogosho";
    private String user2 = "bobi";
    private String password2 = "bobibobi";
    private String postContent = user2 + " posting sample text for User Interactions test case"; //actions.getRandomString(20);

    @Test
    public void test_01_LogIn_User1() {
        actions.waitForElementVisible("logInLink",3);
        actions.logIn();
        actions.assertElementPresent("userNameField");
        actions.assertElementPresent("passwordField");
        actions.assertElementPresent("logInButton");

        // sign in as user1
        actions.signIn(user1, password1);
        actions.assertElementPresent("homepage");
    }

    @Test
    public void test_02_SearchForUser2() {

        // user1 searches for user2
        actions.waitForElementVisible("searchUsersField", 3);
        actions.typeValueInField(user2, "searchUsersField");
        actions.clickElement("searchUsersButton");
        actions.assertTextEquals(user2, "searchResultUsername");

        // user1 views user2s' profile
        actions.clickElement("searchResultLink");
        actions.assertTextEquals("Username: " + user2, "usernameInUserProfilePanel");
        Utils.LOG.info("User " + user2 + " found!");
    }

    @Test
    public void test_03_RequestConnectionToUser() {
        actions.assertElementPresent("friendRequestButton");

        // user1 sends connection request to user2
        actions.clickElement("friendRequestButton");
        actions.assertElementPresent("friendshipRequestedTextInUserProfilePanel");
        Utils.LOG.info(user1 + " requested connection successfully!");
    }

    @Test
    public void test_04_LogOut_User1() {
        // user1 logs out
        actions.logOut();
    }

    @Test
    public void test_05_LogIn_User2() {
        actions.waitForElementVisible("logInLink",3);
        actions.logIn();
        actions.assertElementPresent("userNameField");
        actions.assertElementPresent("passwordField");
        actions.assertElementPresent("logInButton");

        // sign in as user2
        actions.signIn(user2, password2);
        actions.assertElementPresent("homepage");
    }

    @Test
    public void test_06_AcceptFriendRequest() {
        String userRequest = "//td[contains(text(), 'User " + user1 + "')]";
        String userRequestLink = "//td[contains(text(), 'User " + user1 + "')]/following-sibling::td[2]/form/input";
        String connectionConfirmationText = "You are now friends with user " + user1;
        String connectionConfirmationTextLocator = "//div[contains(@class, 'col-lg-8')]/div[2]";

        // user2 views own profile page
        actions.waitForElementVisible("userProfileLink", 3);
        actions.clickElement("userProfileLink");
        actions.assertTextEquals((Utils.getConfigPropertyByKey("updateProfileText")), "updateProfileLink");

        // user2 checks if a connection request is received
        actions.assertElementPresent("viewRequestsLink");
        actions.clickElement("viewRequestsLink");

        // user2 checks if  request from user1 is present
        actions.assertElementPresent(userRequest);
        actions.clickElement(userRequestLink);

        // verify connection request is approved
        actions.assertTextEquals(connectionConfirmationText, connectionConfirmationTextLocator);
        Utils.LOG.info(user2 + " accepted " + user1 + "'s connection request successfully!");
    }

    @Test
    public void test_07_CreatePrivatePost() {

        // user2 goes back to Home page
        actions.clickElement("Home");

        // user2 goes to own profile page
        actions.clickElement("userProfileLink");

        // user2 creates a private post
        actions.waitForElementVisible("postTextArea", 3);
        actions.typeValueInField(postContent, "postTextArea");
        actions.clickElement("privatePostCheckbox");
        actions.clickElement("postButton");
        actions.assertTextEquals(postContent, "latestPostTextInProfilePage");
        Utils.LOG.info(user2 + " created private post successfully!");
    }

    @Test
    public void test_08_LogOut_User2() {
        // user2 logs out
        actions.logOut();
    }

    @Test
    public void test_09_Verify_PrivatePostIsVisibleByFriend() {

        // user1 logs in again
        test_01_LogIn_User1();

        // user1 navigates to Home page
        actions.clickElement("Home");

        // user1 verifies post content is the same as the private post content of user2
        actions.assertTextEquals(postContent, "latestPostText");
        Utils.LOG.info(user2 + " HAS visibility of " + user1 + "'s private post in the Home page feed!");
    }

    @Test
    public void test_10_Like_PrivatePost() {
        String expectedText = "1 likes";

        // verify latest post's content
        actions.assertTextEquals(postContent, "latestPostText");

        //user1 Likes post
        actions.assertElementPresent("latestPostLikeButton");
        actions.clickElement("latestPostLikeButton");
        actions.assertTextEquals(expectedText, "latestPostLikeCounter");
        Utils.LOG.info(user1 + " liked " + user2 + "'s post!");
    }

    @Test
    public void test_11_Unlike_PrivatePost() {
        String expectedText = "0 likes";

        // verify latest post's content
        actions.assertTextEquals(postContent, "latestPostText");

        // user1 unlikes post
        actions.assertElementPresent("latestPostLikeButton");
        actions.clickElement("latestPostLikeButton");
        actions.assertTextEquals(expectedText, "latestPostLikeCounter");
        Utils.LOG.info(user1 + " unliked " + user2 + "'s post!");
    }

    @Test
    public void test_12_Unfriend_User() {

        // user1 searches for user2
        test_02_SearchForUser2();

        // user1 clicks on Unfriend button
        actions.assertElementPresent("unfriendRequestButton");
        actions.clickElement("unfriendRequestButton");

        // verify users are not friends any more
        actions.assertElementPresent("friendRequestButton");
        Utils.LOG.info(user1 + " and " + user2 + " are NOT connected any more!");

        // log out user1
        test_04_LogOut_User1();
    }

    @Test
    public void test_13_DeleteOwnPost() {
        String expectedText = "Username: " + user2;

        test_05_LogIn_User2();

        // user2 navigates to own profile page
        actions.clickElement("userProfileLink");
        actions.assertTextEquals(expectedText, "usernameInUserProfilePanel");

        // verify latest post is present
        actions.assertTextEquals(postContent, "latestPostTextInProfilePage");

        // delete latest post
        actions.clickElement("latestPostDeleteButton");

        // verify latest post is deleted
        try {
            actions.assertTextNotEquals(postContent, "latestPostTextInProfilePage");
            Utils.LOG.info(user2 + " DELETED post successfully!");
        }
        // in case no more posts are present after deleting the post
        catch (org.openqa.selenium.TimeoutException e) {
            Utils.LOG.info(user2 + " DELETED only post successfully!");
        }

        test_08_LogOut_User2();
    }
}
