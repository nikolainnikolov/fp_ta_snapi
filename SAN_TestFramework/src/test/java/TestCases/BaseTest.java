package TestCases;

import com.behavetestframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest {

    public UserActions actions=new UserActions();

    @BeforeClass
    public static void setUp(){
        UserActions.loadBrowser();
    }
    @AfterClass
    public static void tearDown(){
        UserActions.quitDriver();
    }
}
