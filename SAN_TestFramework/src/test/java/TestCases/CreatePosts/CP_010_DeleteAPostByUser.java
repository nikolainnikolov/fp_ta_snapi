package TestCases.CreatePosts;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CP_010_DeleteAPostByUser extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInBase();
    }
    @Test
    public void deleteAPostByUser() {
        actions.waitForElementVisible("userProfileLink", 10);
        actions.clickElement("userProfileLink");

        Utils.LOG.info("Open an existing post to delete it");
        actions.clickElement("postDeleteIcon");
        actions.assertElementNotPresent("postCreatedByUser");

    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}
