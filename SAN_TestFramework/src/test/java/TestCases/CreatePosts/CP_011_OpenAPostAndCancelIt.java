package TestCases.CreatePosts;

import TestCases.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CP_011_OpenAPostAndCancelIt extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInBase();
    }
    @Test
    public void openAPostAndCancelIt() {
        String text="kkk";
        actions.waitForElementVisible("postTextField",10);
        actions.typeValueInField(text, "postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("Home");
        actions.assertTextNotEquals(text, "postCreatedByUser");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }

}
