package TestCases.CreatePosts;

import TestCases.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.annotation.Repeatable;

public class CP_001_CreateAPublicPostWithMinSymbols extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.assertElementPresent("userNameField");
        actions.assertElementPresent("passwordField");
        actions.assertElementPresent("logInButton");
        actions.signInBase();
    }
    @Test
    public void createATopic(){

        actions.waitForElementVisible("postTextField", 10);
        actions.typeRandomStringInField("2", "postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.assertElementPresent("postCreatedByUser");

    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}
