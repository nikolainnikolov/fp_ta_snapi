package TestCases.CreatePosts;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CP_402_CannotCreateAnEmptyPost extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInBase();
    }
    @Test
    public void cannotCreateAnEmptyPost(){

        actions.waitForElementVisible("postTextField", 10);
        actions.typeValueInField("", "postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.assertTextPresent("messageEmptyPost","postField");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}
