package TestCases.CreatePosts;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CP_406_UserCannotEditOtherUsersPost extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInTest();
    }
    @Test
    public void userCannotEditOtherUsersPost() {
        String editedPost="Edited";
        actions.waitForElementVisible("postCreatedByUser", 10);
        actions.clickElement("otherUsersPost");
        Utils.LOG.info("Open an existing post for edit");
        actions.assertElementNotPresent("postEditIcon");

    }
    @After
    public void LogOut(){
        actions.logOut();
    }

}
