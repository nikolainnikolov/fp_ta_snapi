package TestCases.CreatePosts;

import TestCases.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CP_002_CreateAPrivatePostWithMaxSymbols extends BaseTest {

    @Test
    public void createAPrivatePostWithMaxSymbols() {
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInBase();
        String privatePost="PRIVATE123PRIVATE123PRIVATE123PRIVATE123PRIVATE123";
        actions.waitForElementVisible("postTextField", 10);
        actions.typeValueInField(privatePost, "postTextField");
        actions.clickElement("setPrivateVisibility");
        actions.clickElement("createNewPostButton");
        actions.logOut();
        actions.logIn();
        actions.signInTest();
        actions.assertElementPresent("homepage");
        actions.assertTextNotEquals(privatePost,"postCreatedByUser");
        actions.logOut();
    }
}
