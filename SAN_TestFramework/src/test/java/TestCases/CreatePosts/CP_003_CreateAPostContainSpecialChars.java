package TestCases.CreatePosts;

import TestCases.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CP_003_CreateAPostContainSpecialChars extends BaseTest {

    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInBase();
    }
    @Test
    public void createAPostContainSpecialChars (){
        String PostContent="/[ !@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?]/";
        actions.typeValueInField(PostContent,"postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.returnToHomePage();
        actions.assertElementPresent("postCreatedByUser");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}

