package TestCases.CreatePosts;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CP_004_CannotCreateAPostWithLinkAsContent extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInBase();
    }
    @Test
    public void cannotCreateAPostWithLinkAsContent (){
        actions.waitForElementVisible("postTextField", 10);
        actions.typeValueInField(Utils.getConfigPropertyByKey("link"), "postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.assertElementPresent("postCreatedByUser");
        actions.clickElement("postCreatedByUser");
        actions.assertElementNotPresent("GoogleLogo");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}

