package TestCases.CreatePosts;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Posts extends BaseTest {

    @Test
    public void createAPost(){
        actions.logIn();
        actions.signInBase();
        actions.waitForElementVisible("postTextField", 10);
        actions.typeRandomStringInField("2", "postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.assertElementPresent("postCreatedByUser");
        actions.logOut();
    }
    @Test
    public void createAPrivatePostWithMaxSymbols() {
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInBase();
        String privatePost="PRIVATE123PRIVATE123PRIVATE123PRIVATE123PRIVATE123";
        actions.waitForElementVisible("postTextField", 10);
        actions.typeValueInField(privatePost, "postTextField");
        actions.clickElement("setPrivateVisibility");
        actions.clickElement("createNewPostButton");
        actions.logOut();
        actions.logIn();
        actions.signInTest();
        actions.assertElementPresent("homepage");
        actions.assertTextNotEquals(privatePost,"postCreatedByUser");
        actions.logOut();
    }
    @Test
    public void createAPostContainSpecialChars (){
        String PostContent="/[ !@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?]/";
        actions.logIn();
        actions.signInBase();
        actions.typeValueInField(PostContent,"postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.returnToHomePage();
        actions.assertElementPresent("postCreatedByUser");
        actions.logOut();
    }
    @Test
    public void cannotCreateAPostWithLinkAsContent (){
        actions.logIn();
        actions.signInBase();
        actions.waitForElementVisible("postTextField", 10);
        actions.typeValueInField(Utils.getConfigPropertyByKey("link"), "postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.assertElementPresent("postCreatedByUser");
        actions.clickElement("postCreatedByUser");
        actions.assertElementNotPresent("GoogleLogo");
        actions.logOut();
    }
    @Test
    public void impossibleEditAPostByUser() {
        String editedPost="Edited";
        actions.logIn();
        actions.signInBase();
        actions.waitForElementVisible("userProfileLink", 10);
        actions.clickElement("userProfileLink");

        Utils.LOG.info("Open an existing post for edit");

        try{
            actions.assertElementPresent("postEditIcon");
            actions.clickElement("postEditIcon");
            actions.typeValueInField(editedPost, "postTextField");
            actions.clickElement("setPublicVisibility");
            actions.clickElement("createNewPostButton");
            actions.assertTextEquals(editedPost, "postCreatedByUser");
        }catch (Exception e){
            Utils.LOG.info("There is not 'Update Post' button");
        }
        actions.logOut();
    }
    @Test
    public void deleteAPostByUser() {
        actions.logIn();
        actions.signInBase();
        actions.waitForElementVisible("userProfileLink", 10);
        actions.clickElement("userProfileLink");

        Utils.LOG.info("Open an existing post to delete it");
        actions.clickElement("postDeleteIcon");
        actions.assertElementNotPresent("postCreatedByUser");
        actions.logOut();
    }
    @Test
    public void openAPostAndCancelIt() {
        String text="kkk";
        actions.logIn();
        actions.signInBase();
        actions.waitForElementVisible("postTextField",10);
        actions.typeValueInField(text, "postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("Home");
        actions.assertTextNotEquals(text, "postCreatedByUser");
        actions.logOut();
    }
    @Test
    public void unregisteredUserCannotPost(){
        Utils.LOG.info("User is not logged in.");
        actions.waitForElementVisible("homepage",5);
        actions.assertElementNotPresent("postTextField");
        actions.logIn();
        actions.signInTest();
        actions.assertElementPresent("postTextField");
        actions.logOut();
    }

    @Test
    public void createAPostWithoutSetVisibility() {
        String postWithNoSetVisibility="Post";
        actions.logIn();
        actions.signInBase();
        actions.waitForElementVisible("postTextField", 10);
        actions.typeRandomStringInField("20", "postTextField");
        actions.clickElement("createNewPostButton");
        try {
            actions.assertTextNotEquals(postWithNoSetVisibility,"postCreatedByUser");
        }catch (Exception e){
            Utils.LOG.info("There is no warning message for not set post visibility");
        }
        actions.logOut();
    }
    @Test
    public void userCannotEditOtherUsersPost() {
        String editedPost = "Edited";
        actions.logIn();
        actions.signInTest();
        actions.waitForElementVisible("postCreatedByUser", 10);
        actions.clickElement("otherUsersPost");
        Utils.LOG.info("Open an existing post for edit");
        actions.assertElementNotPresent("postEditIcon");
        actions.clickElement("Home");
        actions.logOut();
    }
    @Test
    public void userCannotDeleteOtherUsersPost() {
        actions.logIn();
        actions.signInTest();
        actions.waitForElementVisible("postCreatedByUser", 10);
        actions.clickElement("otherUsersPost");

        Utils.LOG.info("Open an existing post to delete it");
        actions.assertElementNotPresent("postDeleteButton");
        Utils.LOG.info("There is not 'Delete Post' button");
        actions.logOut();
    }
}
