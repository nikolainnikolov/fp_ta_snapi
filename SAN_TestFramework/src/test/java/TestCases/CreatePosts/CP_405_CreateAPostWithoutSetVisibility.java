package TestCases.CreatePosts;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CP_405_CreateAPostWithoutSetVisibility extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInBase();
    }
    @Test
    public void createAPostWithoutSetVisibility() {
        String postWithNoSetVisibility="Post";
        actions.waitForElementVisible("postTextField", 10);
        actions.typeRandomStringInField("20", "postTextField");
        actions.clickElement("createNewPostButton");
        try {
            actions.assertTextNotEquals(postWithNoSetVisibility,"postCreatedByUser");
        }catch (Exception e){
            Utils.LOG.info("There is no warning message for not set post visibility");
        }
    }
    @After
    public void LogOut(){
        actions.logOut();
    }


}
