package TestCases.CreatePosts;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Test;

public class CP_401_UnregisteredUserCannotCreateAPost extends BaseTest {

    @Test
    public void unregisteredUserCannotPost(){
        Utils.LOG.info("User is not logged in.");
        actions.waitForElementVisible("homepage",5);
        actions.assertElementNotPresent("postTextField");
        actions.logIn();
        actions.signInTest();
        actions.assertElementPresent("postTextField");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}
