package TestCases.CreatePosts;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CP_009_ImpossibleEditAPostByUser extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInBase();
    }
    @Test
    public void impossibleEditAPostByUser() {
        String editedPost="Edited";
        actions.waitForElementVisible("userProfileLink", 10);
        actions.clickElement("userProfileLink");

        Utils.LOG.info("Open an existing post for edit");

        try{
            actions.assertElementPresent("postEditIcon");
            actions.clickElement("postEditIcon");
            actions.typeValueInField(editedPost, "postTextField");
            actions.clickElement("setPublicVisibility");
            actions.clickElement("createNewPostButton");
            actions.assertTextEquals(editedPost, "postCreatedByUser");
        }catch (Exception e){
            Utils.LOG.info("There is not 'Update Post' button");
        }
    }
    @After
    public void LogOut(){
        actions.logOut();
    }
    }

