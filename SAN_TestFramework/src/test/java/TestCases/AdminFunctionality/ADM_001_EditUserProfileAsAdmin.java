package TestCases.AdminFunctionality;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ADM_001_EditUserProfileAsAdmin extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInAdmin();
    }
    @Test
    public void editUserProfileAsAdmin(){
        String firstName= "Updated First name";
        String lastName="Updated Last name";
        String updatedName=firstName+" "+lastName;
        actions.waitForElementVisible("searchUsersButton",3);
        actions.clickElement("searchUsersButton");
        actions.waitForElementVisible("searchUserProfile",3);
        actions.clickElement("searchUserProfile");
        actions.assertElementPresent("userProfileReviseInfo");
        actions.clickElement("userProfileReviseInfo");
        actions.typeValueInField(firstName,"userProfileFirstName");
        actions.typeValueInField(lastName,"userProfileLastName");
        actions.clickElement("userProfileUpdateInfoButton");
        actions.clickElement("Home");
        actions.clickElement("searchUsersButton");
        actions.assertTextEquals(updatedName, "searchUserProfile");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}
