package TestCases.AdminFunctionality;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Admin extends BaseTest {

    @Test
    public void ADM_001_EditUserProfileAsAdmin(){
        String firstName= "Updated First name";
        String lastName="Updated Last name";
        String updatedName=firstName+" "+lastName;

        actions.waitForElementVisible("logInLink",3);
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("searchUsersButton",3);
        actions.clickElement("searchUsersButton");
        actions.waitForElementVisible("searchUserProfile",3);
        actions.clickElement("searchUserProfile");
        actions.assertElementPresent("userProfileReviseInfo");
        actions.clickElement("userProfileReviseInfo");
        actions.typeValueInField(firstName,"userProfileFirstName");
        actions.typeValueInField(lastName,"userProfileLastName");
        actions.clickElement("userProfileUpdateInfoButton");
        actions.clickElement("Home");
        actions.clickElement("searchUsersButton");
        actions.assertTextEquals(updatedName, "searchUserProfile");
        actions.logOut();
    }
    @Test
    public void ADM_002_DeleteUserProfileAsAdmin() {
        String username =actions.getRandomString(8);
        String password = actions.getRandomString(8);;
        String passwordConfirmation = password;
        String email =actions.getRandomString(5)+"@mail.bg";

        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(email, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(passwordConfirmation, "registerConfirmPasswordField");
        actions.clickElement("registerButton");

        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("searchUsersField",3);
        actions.typeValueInField(username,"searchUsersField");
        actions.clickElement("searchUsersButton");
        actions.waitForElementVisible("searchedUserByName",3);
        actions.clickElement("searchedUserByName");
        actions.assertElementPresent("userProfileDeleteUser");
        actions.clickElement("userProfileDeleteUser");
        actions.clickElement("Home");
        actions.clickElement("searchUsersButton");
        actions.assertTextNotEquals(username, "searchUsersList");
        actions.logOut();
    }
    @Test
    public void ADM_003_AdminEditUsersPost() {
        String editedPost="New post";
        String postForEdit= "Updated by Admin";

        actions.waitForElementVisible("logInLink", 3);
        actions.logIn();
        actions.signInBase();
        actions.typeValueInField(postForEdit,"postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.logOut();
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("searchUsersField",3);
        actions.typeValueInField(Utils.getConfigPropertyByKey("username"),"searchUsersField");
        actions.clickElement("searchUsersButton");
        actions.waitForElementVisible("lastPostCreated", 3);
        actions.clickElement("postEditIcon");
        actions.typeValueInField(editedPost, "postEditTextField");
        actions.clickElement("postEditIcon");
        actions.clickElement("Home");
        actions.assertTextEquals(editedPost, "updatedPostText");
        actions.logOut();
    }
    @Test
    public void ADM_004_AdminDeleteUsersPost(){
        String postForDeletion= "Admin will delete this post";

        actions.waitForElementVisible("logInLink", 3);
        actions.logIn();
        actions.signInTest();
        actions.typeValueInField(postForDeletion,"postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.logOut();
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("lastPostCreated", 3);
        actions.clickElement("postDeleteButton");
        actions.assertTextNotEquals(postForDeletion,"lastPostCreated");
        actions.logOut();
    }
    @Test
    public void ADM_005_AdminEditUsersComment(){
        String usersComment="Admin will update this comment";
        String editedComment="Admin changes the content of this comment";

        actions.waitForElementVisible("logInLink", 3);
        actions.logIn();
        actions.signInTest();
        actions.waitForElementVisible("lastPostCreated", 10);
        actions.typeValueInField(usersComment,"postCommentField");
        actions.clickElement("createCommentButton");
        actions.logOut();
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("lastPostCreated", 10);
        actions.clickElement("commentEditButton");
        actions.typeValueInField(editedComment, "postEditTextField");
        actions.clickElement("commentEditButton");
        actions.assertTextEquals(editedComment, "updatedCommentText");
        actions.logOut();
    }
    @Test
    public void ADM_006_AdminDeleteUsersComment() {
        String usersComment = "Admin will delete this comment";

        actions.waitForElementVisible("logInLink", 3);
        actions.logIn();
        actions.signInTest();
        actions.waitForElementVisible("lastPostCreated", 10);
        actions.typeValueInField(usersComment,"postCommentField");
        actions.clickElement("createCommentButton");
        actions.logOut();
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("lastPostCreated", 10);
        actions.assertElementPresent("commentDeleteButton");
            actions.clickElement("commentDeleteButton");
            actions.assertTextNotEquals(usersComment,"updatedCommentText");
    }
}
