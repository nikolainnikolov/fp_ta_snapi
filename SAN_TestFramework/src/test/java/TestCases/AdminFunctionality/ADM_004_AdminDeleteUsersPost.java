package TestCases.AdminFunctionality;

import TestCases.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ADM_004_AdminDeleteUsersPost extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",3);
        actions.logIn();
        actions.signInTest();
    }
    @Test
    public void adminDeleteUsersPost(){
        String postForDeletion= "Admin will delete this post";
        actions.typeValueInField(postForDeletion,"postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.logOut();
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("lastPostCreated", 3);
        actions.clickElement("postDeleteButton");
       actions.assertTextNotEquals(postForDeletion,"lastPostCreated");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}
