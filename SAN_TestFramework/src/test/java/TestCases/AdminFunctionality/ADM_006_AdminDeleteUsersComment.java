package TestCases.AdminFunctionality;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ADM_006_AdminDeleteUsersComment extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInTest();
    }
    @Test
    public void adminDeleteUsersComment() {
        String usersComment = "Admin will delete this comment";
        actions.waitForElementVisible("lastPostCreated", 10);
        actions.typeValueInField(usersComment,"postCommentField");
        actions.clickElement("createCommentButton");
        actions.logOut();
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("lastPostCreated", 10);
        actions.verifyElementVisible("commentDeleteButton");
        actions.clickElement("commentDeleteButton");
        actions.assertTextNotEquals(usersComment,"updatedCommentText");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}
