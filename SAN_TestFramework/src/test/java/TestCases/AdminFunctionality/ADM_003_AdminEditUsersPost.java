package TestCases.AdminFunctionality;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ADM_003_AdminEditUsersPost extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",3);
        actions.logIn();
        actions.signInTest();
    }
    @Test
public void adminEditUsersPost(){
        actions.typeValueInField(Utils.getConfigPropertyByKey("adminWillUpdateThisPost"),"postTextField");
        actions.clickElement("setPublicVisibility");
        actions.clickElement("createNewPostButton");
        actions.logOut();
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("searchUsersField",3);
        actions.typeValueInField(Utils.getConfigPropertyByKey("usernameTest"),"searchUsersField");
        actions.clickElement("searchUsersButton");
        actions.waitForElementVisible("lastPostCreated", 3);
        actions.clickElement("postEditIcon");
        actions.typeValueInField(Utils.getConfigPropertyByKey("adminNewPost"), "postEditTextField");
        actions.clickElement("postEditIcon");
        actions.clickElement("Home");
        actions.assertTextEquals(Utils.getConfigPropertyByKey("adminNewPost"), "updatedPostText");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}
