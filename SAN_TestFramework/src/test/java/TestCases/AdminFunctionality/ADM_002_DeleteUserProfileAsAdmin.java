package TestCases.AdminFunctionality;

import TestCases.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ADM_002_DeleteUserProfileAsAdmin extends BaseTest {

    @Test
    public void deleteUserProfileAsAdmin() {
        String username =actions.getRandomString(8);
        String password = actions.getRandomString(8);;
        String passwordConfirmation = password;
        String email =actions.getRandomString(5)+"@mail.bg";

        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(email, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(passwordConfirmation, "registerConfirmPasswordField");
        actions.clickElement("registerButton");

        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("searchUsersField",3);
        actions.typeValueInField(username,"searchUsersField");
        actions.clickElement("searchUsersButton");
        actions.waitForElementVisible("searchedUserByName",3);
        actions.clickElement("searchedUserByName");
        actions.assertElementPresent("userProfileDeleteUser");
        actions.clickElement("userProfileDeleteUser");
        actions.clickElement("Home");
        actions.clickElement("searchUsersButton");
        actions.assertTextNotEquals(username, "searchUsersList");

    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}