package TestCases.AdminFunctionality;

import TestCases.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ADM_005_AdminEditUsersComment extends BaseTest {
    @Before
    public void logIn(){
        actions.waitForElementVisible("logInLink",10);
        actions.logIn();
        actions.signInTest();
    }
    @Test
    public void adminEditUsersComment(){
        String usersComment="Admin will update this comment";
        String editedComment="Admin changes the content of this comment";
        actions.waitForElementVisible("lastPostCreated", 10);
        actions.typeValueInField(usersComment,"postCommentField");
        actions.clickElement("createCommentButton");
        actions.logOut();
        actions.logIn();
        actions.signInAdmin();
        actions.waitForElementVisible("lastPostCreated", 10);
        actions.clickElement("commentEditButton");
        actions.typeValueInField(editedComment, "postEditTextField");
        actions.clickElement("commentEditButton");
        actions.assertTextEquals(editedComment, "updatedCommentText");
    }
    @After
    public void LogOut(){
        actions.logOut();
    }

}
