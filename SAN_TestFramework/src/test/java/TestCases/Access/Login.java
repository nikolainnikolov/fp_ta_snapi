package TestCases.Access;

import TestCases.BaseTest;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.junit.After;
import org.junit.Test;

public class Login extends BaseTest {

    @After
    public void afterEachTest() {
        actions.clickElement("Home");
    }

    @Test
    public void lg_001_verifySuccessfulLogin() {
        String username = "NovemberRain";
        String password = "novemberrain";

        actions.clickElement("logInLink");
        actions.typeValueInField(username, "userNameField");
        actions.typeValueInField(password, "passwordField");
        actions.clickElement("logInButton");
        actions.assertElementPresent("homepage");
        actions.clickElement("logOutLink");
    }

    @Test
    public void lg_401_passEmptyStringAsPassword() {
        String username = "NovemberRain";
        String password = "";

        actions.clickElement("logInLink");
        actions.typeValueInField(username, "userNameField");
        actions.typeValueInField(password, "passwordField");
        actions.clickElement("logInButton");
        actions.assertElementNotPresent("homepage");
    }

    @Test
    public void lg_402_passEmptyStringAsUsername() {
        String username = "";
        String password = "novemberrain";

        actions.clickElement("logInLink");
        actions.typeValueInField(username, "userNameField");
        actions.typeValueInField(password, "passwordField");
        actions.clickElement("logInButton");
        actions.assertElementNotPresent("homepage");
    }

    @Test
    public void lg_403_loginWithWrongPassword() {
        String username = "NovemberRain";
        String wrongPassword = "novemberain";

        actions.clickElement("logInLink");
        actions.typeValueInField(username, "userNameField");
        actions.typeValueInField(wrongPassword, "passwordField");
        actions.clickElement("logInButton");
        actions.assertElementNotPresent("homepage");
    }

    @Test
    public void lg_404_loginWithWrongUsername() {
        String wrongUsername = "pesho";
        String password = "novemberrain";

        actions.clickElement("logInLink");
        actions.typeValueInField(wrongUsername, "userNameField");
        actions.typeValueInField(password, "passwordField");
        actions.clickElement("logInButton");
        actions.assertElementNotPresent("homepage");
    }
    @Test
    public void lg_405_nonExistingBothUsernameAndPassword() {
        String wrongUsername = "Super";
        String password = "marketplace";

        actions.clickElement("logInLink");
        actions.typeValueInField(wrongUsername, "userNameField");
        actions.typeValueInField(password, "passwordField");
        actions.clickElement("logInButton");
        actions.assertElementNotPresent("homepage");
    }
    @Test
    public void lg_002_loginWithEmailAndPassword() {
        String wrongUsername ="ludotopile@mail.bg";
        String password = "ludotopile";

        actions.clickElement("logInLink");
        actions.typeValueInField(wrongUsername, "userNameField");
        actions.typeValueInField(password, "passwordField");
        actions.clickElement("logInButton");
        actions.assertElementPresent("homepage");
        actions.logOut();
    }
}
