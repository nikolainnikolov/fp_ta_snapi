package TestCases.Access;

import TestCases.BaseTest;
import org.junit.After;
import org.junit.Test;

public class Registration extends BaseTest {

    String username=actions.getRandomString(5);
    String password = actions.getRandomString(8);
    String passwordUnderMin=actions.getRandomString(7);
    String passwordConfirmation = password;
    String passwordNoConfirmation="00000000";
    String email = actions.getRandomString(5)+"@kocho.bg";
    String emailInvalid= actions.getRandomString(5)+"@mail";
    String empty = "";

    @After
    public void afterEachTest() {
        actions.clickElement("Home");
    }

    @Test
    public void rg_003_verifyNoRegistrationWithEmptyStringForUsername() {
        actions.clickElement("registerLink");
        actions.typeValueInField(empty, "registerUsernameField");
        actions.typeValueInField(email, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(passwordConfirmation, "registerConfirmPasswordField");
        actions.clickElement("registerButton");
        actions.assertElementNotPresent("successfulRegistrationMessage");
    }
    @Test
    public void rg_004_verifyNoRegistrationWithEmptyStringForEmail() {
        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(empty, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(passwordConfirmation, "registerConfirmPasswordField");
        actions.clickElement("registerButton");
        actions.assertElementNotPresent("successfulRegistrationMessage");
    }
    @Test
    public void rg_005_verifyNoRegistrationWithEmptyStringForPassword() {
        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(email, "registerEmailField");
        actions.typeValueInField(empty, "registerPasswordField");
        actions.typeValueInField(passwordConfirmation, "registerConfirmPasswordField");
        actions.clickElement("registerButton");
        actions.assertElementNotPresent("successfulRegistrationMessage");
    }
    @Test
    public void rg_006_verifyNoRegistrationWithEmptyStringForPasswordConfirm() {
        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(email, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(empty, "registerConfirmPasswordField");
        actions.clickElement("registerButton");
        actions.assertElementNotPresent("successfulRegistrationMessage");
    }

    @Test
    public void rg_009_verifyNoRegistrationWithInvalidEmail() {
        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(emailInvalid, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(passwordConfirmation, "registerConfirmPasswordField");
        actions.clickElement("registerButton");
        actions.assertElementNotPresent("successfulRegistrationMessage");
    }

    @Test
    public void rg_010_verifyNoRegistrationWhenPasswordsMismatch() {
        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(email, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(passwordNoConfirmation, "registerConfirmPasswordField");
        actions.clickElement("registerButton");
        actions.assertElementNotPresent("successfulRegistrationMessage");
        actions.assertElementPresent("passwordMismatchMessage");
    }

    @Test
    public void rg_002_verifySuccessfulRegistration() {
        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(email, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(passwordConfirmation, "registerConfirmPasswordField");
        actions.clickElement("registerButton");
        actions.assertElementPresent("successfulRegistrationMessage");
    }

}
