package TestCases.UserProfileManagement;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import javafx.scene.layout.Priority;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.Keys;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UPM_01_UserVerifiesAndUpdatesProfileInfo extends BaseTest {

    String username ="AmBVll1";
    String email ="anhv01@mail.bg";
    String password = "010g800bv89a";
    String passwordConfirmation =password;

    @Test
    public void UPM_010_UserVerifiesAndUpdatesProfileInfo() {

        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(email, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(passwordConfirmation, "registerConfirmPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("firstNameNewUser"), "registerFirstNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("lastNameNewUser"), "registerLastNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("dateNewUser"), "registerBirthDateField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("addressNewUser"), "registerAddressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("cityOfBirthNewUser"), "registerCityOfBirthField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("planetOfBirthNewUser"), "registerPlanetField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("cityOfResidenceNewUser"), "registerCityOfResidenceField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("planetOfResidenceNewUser"), "registerPlanetOfResidenceField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("jobNewUser"), "registerJobTitleField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("educationNewUser"), "registerEducationLevelField");
        actions.clickElement("registerButton");
      actions.assertElementPresent("successfulRegistrationMessage");
        actions.assertElementPresent("logInLink");
        actions.logIn();
        actions.assertElementPresent("userNameField");
        actions.assertElementPresent("passwordField");
        actions.assertElementPresent("logInButton");
        try {
            actions.typeValueInField(username, "userNameField");
            actions.typeValueInField(password, "passwordField");
            actions.clickElement("logInButton");
        } catch (Exception e) {
            Utils.LOG.info("Already logged in");
        }

        actions.assertElementPresent("userProfileLink");
        actions.clickElement("userProfileLink");

        actions.assertElementPresent("userProfilePicture");
        actions.assertTextEquals("First name: " + Utils.getConfigPropertyByKey("firstNameNewUser"), "userFirstNameInUserProfilePanel");
        actions.assertTextEquals("Last name: " + Utils.getConfigPropertyByKey("lastNameNewUser"), "userLastNameInUserProfilePanel");
        actions.assertTextEquals("Username: " + username, "usernameInUserProfilePanel");
        actions.assertTextEquals("Email: " + email, "userEmailInUserProfilePanel");
        actions.assertTextEquals("Education level: " + Utils.getConfigPropertyByKey("educationNewUser"), "userEducationINProfilePanel");
        actions.assertTextEquals("Job title: " + Utils.getConfigPropertyByKey("jobNewUser"), "userJobINProfilePanel");
        actions.assertTextEquals("City of Residence: " + Utils.getConfigPropertyByKey("cityOfResidenceNewUser"), "userCityOfResidenceINProfilePanel");
        actions.assertTextEquals("City of Birth: " + Utils.getConfigPropertyByKey("cityOfBirthNewUser"), "userCityOfBirthINProfilePanel");
        actions.assertTextEquals("Address: " + Utils.getConfigPropertyByKey("addressNewUser"), "userAddressINProfilePanel");

        Utils.LOG.info("User is going to update the personal information on the Profile page");
        actions.clickElement("userProfileLink");
        actions.waitForElementVisible("userProfileFirstName", 3);
        actions.typeValueInField(Utils.getConfigPropertyByKey("updatedFirstName"), "userProfileFirstName");
        actions.typeValueInField(Utils.getConfigPropertyByKey("updatedLastName"), "userProfileLastName");
        actions.typeValueInField(Utils.getConfigPropertyByKey("updatedEmail"), "userProfileEmail");
        actions.typeValueInField(Utils.getConfigPropertyByKey("updatedAddress"), "userProfileAddress");
        actions.typeValueInField(Utils.getConfigPropertyByKey("updatedCityOfBirth"), "userProfileCityOfBirth");
        actions.typeValueInField(Utils.getConfigPropertyByKey("updatedPlanetOfBirth"), "userProfilePlanetOfCityOfBirth");
        actions.typeValueInField(Utils.getConfigPropertyByKey("updatedCityOfResidence"), "userProfileCityOfResidence");
        actions.typeValueInField(Utils.getConfigPropertyByKey("updatedPlanetOfResidence"), "userProfilePlanetOfCityOfResidence");
        actions.typeValueInField(Utils.getConfigPropertyByKey("updatedJob"), "userProfileJobTitle");
        actions.typeValueInFieldAndPressKey(Utils.getConfigPropertyByKey("updatedEducation"), "userProfileEducationLevel", Keys.ENTER);
        actions.assertElementPresent("successfulUpdateProfileMessage");
        Utils.LOG.info("User updated the personal information on the Profile page");
           }
    @Test
    public void UPM_020_UserVerifiesUpdatedFirstNameInfoInProfile() {
       actions.waitForElementVisible("userProfileLink",3);
        actions.clickElement("userProfileLink");
        actions.assertElementPresent("usernameInUserProfilePanel");
        actions.assertTextEquals("Username: " + username, "usernameInUserProfilePanel");
        actions.assertTextEquals("First name: " + Utils.getConfigPropertyByKey("updatedFirstName"), "userFirstNameInUserProfilePanel");
    }
    @Test
    public void UPM_021_UserVerifiesUpdatedLastNameInfoInProfile() {
        actions.assertTextEquals("Username: " + username, "usernameInUserProfilePanel");
        actions.assertTextEquals("Last name: " + Utils.getConfigPropertyByKey("updatedLastName"), "userLastNameInUserProfilePanel");
    }
    @Test
    public void UPM_022_UserVerifiesUpdatedEmailInfoInProfile() {
        actions.assertTextEquals("Username: " + username, "usernameInUserProfilePanel");
        actions.assertTextEquals("Email: " + Utils.getConfigPropertyByKey("updatedEmail"), "userEmailInUserProfilePanel");
    }
    @Test
    public void UPM_023_UserVerifiesUpdatedEducationInfoInProfile() {
        actions.assertTextEquals("Username: " + username, "usernameInUserProfilePanel");
        actions.assertTextEquals("Education level: " + Utils.getConfigPropertyByKey("updatedEducation"), "userEducationINProfilePanel");
    }
    @Test
    public void UPM_024_UserVerifiesUpdatedJobInfoInProfile() {
        actions.assertTextEquals("Username: " + username, "usernameInUserProfilePanel");
        actions.assertTextEquals("Job title: " + Utils.getConfigPropertyByKey("updatedJob"), "userJobINProfilePanel");
    }
    @Test
    public void UPM_025_UserVerifiesUpdatedCityOfResidenceInfoInProfile() {
        actions.assertTextEquals("Username: " + username, "usernameInUserProfilePanel");
        actions.assertTextEquals("City of Residence: " + Utils.getConfigPropertyByKey("updatedCityOfResidence"), "userCityOfResidenceINProfilePanel");
    }
    @Test
    public void UPM_026_UserVerifiesUpdatedCityOfBirthInfoInProfile() {
        actions.assertTextEquals("Username: " + username, "usernameInUserProfilePanel");
        actions.assertTextEquals("City of Birth: " + Utils.getConfigPropertyByKey("updatedCityOfBirth"), "userCityOfBirthINProfilePanel");
    }
    @Test
    public void UPM_027_UserVerifiesUpdatedCityOfBirthInfoInProfile() {
        actions.assertTextEquals("Username: " + username, "usernameInUserProfilePanel");
        actions.assertTextEquals("Address: " + Utils.getConfigPropertyByKey("updatedAddress"), "userAddressINProfilePanel");
    }
}