package TestCases.UserProfileManagement;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import javafx.scene.layout.Priority;
import org.junit.Before;
import org.junit.Test;

public class UPM_003_UserUploadAProfilePicture extends BaseTest {
    @Before
    public void logIn() {
        actions.waitForElementVisible("logInLink", 10);
        actions.logIn();
        actions.signInBase();
    }

    @Test
    public void EditAPostByUser() {
        actions.waitForElementVisible("userProfileLink", 10);
        actions.clickElement("userProfileLink");
        actions.clickElement("userProfilePicture");
        actions.waitForElementVisible("userProfileUpdatePicture",10);
        actions.clickElement("userProfileUpdatePicture");
        actions.uploadPicture("userProfileUploadPictureField");
        actions.clickElement("Home");
        actions.clickElement("userProfileLink");
        actions.assertElementPresent(Utils.getConfigPropertyByKey("ProfilePicture"));
    }
}