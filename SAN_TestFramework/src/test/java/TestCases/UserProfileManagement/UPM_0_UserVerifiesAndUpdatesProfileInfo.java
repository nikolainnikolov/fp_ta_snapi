package TestCases.UserProfileManagement;

import TestCases.BaseTest;
import com.behavetestframework.Utils;
import javafx.scene.layout.Priority;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UPM_0_UserVerifiesAndUpdatesProfileInfo extends BaseTest {
    @Test
    public void UserVerifiesAndUpdatesProfileInfo() {
        String username = "55555555";
        String email = "55555555@mail.bg";
        String password = "55555555";
        String passwordConfirmation = "55555555";

        actions.clickElement("registerLink");
        actions.typeValueInField(username, "registerUsernameField");
        actions.typeValueInField(email, "registerEmailField");
        actions.typeValueInField(password, "registerPasswordField");
        actions.typeValueInField(passwordConfirmation, "registerConfirmPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("firstNameNewUser"), "registerFirstNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("lastNameNewUser"), "registerLastNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("dateNewUser"), "registerBirthDateField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("addressNewUser"), "registerAddressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("cityOfBirthNewUser"), "registerCityOfBirthField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("planetOfBirthNewUser"), "registerPlanetField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("cityOfResidenceNewUser"), "registerCityOfResidenceField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("planetOfResidenceNewUser"), "registerPlanetField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("jobNewUser"), "registerJobTitleField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("educationNewUser"), "registerEducationLevelField");
        actions.clickElement("registerButton");
        actions.assertElementPresent("successfulRegistrationMessage");
        actions.assertElementPresent("logInLink");
        actions.logIn();
        actions.assertElementPresent("userNameField");
        actions.assertElementPresent("passwordField");
        actions.assertElementPresent("logInButton");
        try {
            actions.typeValueInField(username, "userNameField");
            actions.typeValueInField(password, "passwordField");
            actions.clickElement("logInButton");
        } catch (Exception e) {
            Utils.LOG.info("Already logged in");
        }
        actions.assertElementPresent("userProfileLink");
        actions.clickElement("userProfileLink");

        actions.assertElementPresent("userProfilePicture");
        actions.assertTextEquals("First name: "+Utils.getConfigPropertyByKey("firstNameNewUser"),"userFirstNameInUserProfilePanel");
        actions.assertTextEquals("Last name: "+Utils.getConfigPropertyByKey("lastNameNewUser"),"userLastNameInUserProfilePanel");
        actions.assertTextEquals("Username: "+username, "usernameInUserProfilePanel");
        actions.assertTextEquals("Email: "+email,"userEmailInUserProfilePanel");
        actions.assertTextEquals("Education level: "+Utils.getConfigPropertyByKey("educationNewUser"),"userEducationINProfilePanel");
        actions.assertTextEquals("Job title: "+Utils.getConfigPropertyByKey("jobNewUser"),"userJobINProfilePanel");
        actions.assertTextEquals("City of Residence: "+Utils.getConfigPropertyByKey("cityOfResidenceNewUser"),"userCityOfResidenceINProfilePanel");
        actions.assertTextEquals("City of Birth: "+Utils.getConfigPropertyByKey("cityOfBirthNewUser"),"userCityOfBirthINProfilePanel");
        actions.assertTextEquals("Address: "+Utils.getConfigPropertyByKey("addressNewUser"),"userAddressINProfilePanel");


        Utils.LOG.info("User is going to update the personal information on the Profile page");
        actions.clickElement("userProfileLink");
        actions.waitForElementVisible("userProfileFirstName",3);
        actions.typeValueInField("updatedFirstName","userProfileFirstName");
        actions.typeValueInField("updatedLastName","userProfileLastName");
        actions.typeRandomStringInField("updatedEmail","userProfileEmail");
        actions.typeValueInField("updatedAddress","userProfileAddress");
        actions.typeValueInField("updatedDate","userProfileBirthDate");
        actions.typeValueInField("updatedCityOfBirth","userProfileCityOfBirth");
        actions.typeValueInField("updatedPlanetOfBirth","userProfilePlanetOfCityOfBirth");
        actions.typeValueInField("updatedCityOfResidence","userProfileCityOfResidence");
        actions.typeValueInField("updatedPlanetOfResidence","userProfilePlanetOfCityOfResidence");
        actions.typeValueInField("updatedJob","userProfileJobTitle");
        actions.typeValueInField("updatedEducation","userProfileEducationLevel");
        actions.clickElement("userProfileUpdateInfoButton");
        Utils.LOG.info("User updated the personal information on the Profile page");

        actions.assertElementPresent("userProfilePanel");
        actions.assertTextEquals("First name: "+Utils.getConfigPropertyByKey("updatedFirstName"),"userFirstNameInUserProfilePanel");
        actions.assertTextEquals("Last name: "+Utils.getConfigPropertyByKey("updatedLastName"),"userLastNameInUserProfilePanel");
        actions.assertTextEquals("Email: "+Utils.getConfigPropertyByKey("updatedEmail"),"userEmailInUserProfilePanel");
        actions.assertTextEquals("Education level: "+Utils.getConfigPropertyByKey("updatedEducation"),"userEducationINProfilePanel");
        actions.assertTextEquals("Job title: "+Utils.getConfigPropertyByKey("updatedJob"),"userJobINProfilePanel");
        actions.assertTextEquals("City of Residence: "+Utils.getConfigPropertyByKey("updatedCityOfResidence"),"userCityOfResidenceINProfilePanel");
        actions.assertTextEquals("City of Birth: "+Utils.getConfigPropertyByKey("updatedCityOfBirth"),"userCityOfBirthINProfilePanel");
        actions.assertTextEquals("Address: "+Utils.getConfigPropertyByKey("updatedAddress"),"userAddressINProfilePanel");


    }
    @After
    public void LogOut(){
        actions.logOut();
    }
}