package stepdefinitions;

import com.behavetestframework.UserActions;
import com.behavetestframework.Utils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();

    @Given("Current user logs out")
    @When("Current user logs out")
    @Then("Current user logs out")
    public void logOut() {
        actions.logOut();
    }

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element) {
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeValueInField(String value, String field){
        actions.typeValueInField(Utils.getConfigPropertyByKey(value), field);
    }

    @Given("Type $value in $name field and press $key")
    @When("Type $value in $name field and press $key")
    @Then("Type $value in $name field and press $key")
    public void typeValueInFieldAndPressKey(String value, String field, Keys key)  {
        actions.typeValueInFieldAndPressKey(value, field,key);
    }

    @Given("Enter text with $number symbols in $field field")
    @When("Enter text with $number symbols in $field field")
    @Then("Enter text with $number symbols in $field field")
    public void typeRandomStringInField(String number, String field)  {
        int num = Integer.parseInt(number);
        String text = actions.getRandomString(num);
        actions.typeValueInField(text,field );
    }

    // Same as clickElement()
    @Given("Open $locator")
    @When("Open $locator")
    @Then("Open $locator")
    public void openPage(String locator) {
        actions.clickElement(locator);
    }

    // ### WAIT ###

    @Given ("Wait $locator to present for $seconds seconds")
    @When("Wait $locator to present for $seconds seconds")
    @Then("Wait $locator to present for $seconds seconds")
    public void waitForElementVisible(String locator, int seconds) {
        actions.waitForElementVisible(locator, seconds);
    }

    // ### VERIFY ###

    @Given("$locator is visible")
    @When("I verify $locator is visible")
    @Then("Verify $locator is visible")
    public void verifyElementVisible(String locator) {
        actions.verifyElementVisible(locator);
    }

    @Given("Verify $text is equal to $locator")
    @When("Verify $text is equal to $locator")
    @Then("Verify $text is equal to $locator")
    public void verifyTextIsEqualToLocator(String text, String locator) {
        actions.verifyTextIsEqualToLocator(text, locator);
    }

    // ### ASSERT ###

    @Given("Element $locator is present")
    @When("Element $locator is present")
    @Then("Element $locator is present")
    public void assertElementPresent(String locator){
        actions.assertElementPresent(locator);
    }

    @Given("Element $locator is not present")
    @When("Element $locator is not present")
    @Then("Element $locator is not present")
    public void assertElementNotPresent(String locator){
        actions.assertElementNotPresent(locator);
    }

    @Given("Element $expectedText is equal to $locator element")
    @When("Element $expectedText is equal to $locator element")
    @Then("Element $expectedText is equal to $locator element")
    public void assertTextEquals(String expectedText, String locator){
        actions.assertTextEquals(expectedText,locator);
    }

    @Given("Element $expectedText is not exists in $locator")
    @When("Element $expectedText is not exists in $locator")
    @Then("Element $expectedText is not exists in $locator")
    public void assertTextNotEquals(String expectedText, String locator) {
        actions.assertTextNotEquals(expectedText, locator);
    }
}

