Meta:
@UserInteractions

Narrative:
As a  registered user
I want to send a connection request
So that I can see users' private posts

Scenario: Log in as user1, search for user2 and send connection request. Log out.
          Log in as user2, approve the request and create a private post. Log out.
          Log in as user1, verify private post of user2 is visible, search for user2
            and unfriend user2. Log out.

GivenStories: stories/Access/Login_User.story

Given Element searchUsersField is present
And Element searchUsersButton is present
When Type username1 in searchUsersField field
And Click searchUsersButton element
And I verify searchResultLink is visible
And Click searchResultLink element
Then Verify Username: bobi is equal to usernameInUserProfilePanel
And Verify friendRequestButton is visible
Then Click friendRequestButton element

Then Click logOutLink element

Given Element logInLink is present
When Click logInLink element
Then Type username1 in userNameField field
Then Type password1 in passwordField field
Then Click logInButton element
Then Element userProfileLink is present

Given userProfileLink is visible
When Click userProfileLink element
And Verify Username: bobi is equal to usernameInUserProfilePanel
And I verify viewRequestsLink is visible
Then Click viewRequestsLink element
When I verify userNovemberRainConnectionRequest is visible
Then Click userNovemberRainConnectionRequestApproveButton element
And Verify userNovemberRainConnectionRequestConfirmation is visible

Given Click Home element
And postTextField is visible
When Type examplePostText in postTextField field
And Click privatePostCheckbox element
Then Click postButton element
And Verify latestPostText is visible

Then Click logOutLink element

Given Element logInLink is present
When Click logInLink element
Then Type username in userNameField field
Then Type password in passwordField field
Then Click logInButton element
Then Element userProfileLink is present

Given postTextField is visible
Then Verify latestPostText is visible

Given Element searchUsersField is present
And Element searchUsersButton is present
When Type username1 in searchUsersField field
And Click searchUsersButton element
And I verify searchResultLink is visible
And Click searchResultLink element
Then Verify Username: bobi is equal to usernameInUserProfilePanel
And Verify unfriendRequestButton is visible
Then Click unfriendRequestButton element
Then Verify friendRequestButton is visible

And Click logOutLink element