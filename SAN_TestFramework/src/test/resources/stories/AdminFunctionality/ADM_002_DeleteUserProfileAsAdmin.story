Meta:
@Admin

Narrative:
As an Admin
I want to delete user's profile
So that I can protect the SAN from unappropriate content

Scenario: Admin delete user's profile


Given Element searchUsersField is present
When Type userForDeleteTest in searchUsersField field and press ENTER
And Wait searchUserProfile to present for 10 seconds
And Click searchUserProfile element
And Wait userProfileDeleteUser to present for 10 seconds
And Click userProfileDeleteUser element
And Open Home
And Click searchUsersButton element
Then Element userForDeleteTest is not exists in searchUsersList
And Current user logs out