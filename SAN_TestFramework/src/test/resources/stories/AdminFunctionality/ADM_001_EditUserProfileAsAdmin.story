Meta:
@Admin

Narrative:
As an Admin
I want to edit user's profile
So that I can provide appropriate user presentation in the SAN

Scenario: Admin edit user's first name and last name in user's profile


Given Element logInLink is present
When Click logInLink element
And Type usernameAdmin in userNameField field
And Type passwordAdmin in passwordField field
And Click logInButton element
Then Element homepage is present

Given Element searchUsersButton is present
When Click searchUsersButton element
And Wait searchUserProfile to present for 3 seconds
And Click searchUserProfile element
And Wait userProfileReviseInfo to present for 3 seconds
And Click userProfileReviseInfo element
And Type adminUpdatesFirstName in userProfileFirstName field
And Type adminUpdatedLastName in userProfileLastName field
And Click userProfileUpdateInfoButton element
And Click Home element
And Click searchUsersButton element
Then Wait searchUserProfile to present for 3 seconds
And Element Updated First name Updated Last name is equal to searchUserProfile element