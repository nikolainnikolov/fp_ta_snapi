Meta:
@Admin
Narrative:
As a user
I want to login the Social Alian Network
So that I can have an access to the Social Alian Network

Scenario: User login the SAN
Given Element logInLink is present
When Click logInLink element
And Type usernameAdmin in userNameField field
And Type passwordAdmin in passwordField field
And Click logInButton element
Then Element homepage is present