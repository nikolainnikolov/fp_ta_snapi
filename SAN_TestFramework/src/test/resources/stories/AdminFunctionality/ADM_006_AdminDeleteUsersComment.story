Meta:
@Admin

Narrative:
As an Admin
I want to delete user's comment'
So that I can protect the SAN from unappropriate content

Scenario: Admin delete user's comment
Given Element logInLink is present
When Click logInLink element
And Type usernameTest in userNameField field
And Type passwordTest in passwordField field
And Click logInButton element
And Type adminWillDeleteThisPost in postCommentField field
And Click createCommentButton element
And Current user logs out
And Click logInLink element
And Type usernameAdmin in userNameField field
And Type passwordAdmin in passwordField field
And Click logInButton element
And Wait lastPostCreated to present for 10 seconds
Then Verify commentDeleteButton is visible
And Current user logs out