Meta:
@Admin

Narrative:
As an Admin
I want to edit user's post
So that I can provide appropriate content for users in the SAN

Scenario: Admin edit user's post

Given Element logInLink is present
When Click logInLink element
And Type usernameTest in userNameField field
And Type passwordTest in passwordField field
And Click logInButton element
And Type adminWillUpdateThisPost in postTextField field
And Click setPublicVisibility element
And Click createNewPostButton element
And Current user logs out
And Click logInLink element
And Type usernameAdmin in userNameField field
And Type passwordAdmin in passwordField field
And Click logInButton element
And Wait searchUsersField to present for 3 seconds
And Type usernameTest in searchUsersField field
And Click searchUsersButton element
And Wait lastPostCreated to present for 3 seconds
And Click postEditIcon element
And Type adminNewPost in postEditTextField field
And Click postEditIcon element
And Click Home element
Then Element adminNewPost is not exists in updatedPostText
And Current user logs out