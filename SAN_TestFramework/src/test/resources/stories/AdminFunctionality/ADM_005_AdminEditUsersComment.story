Meta:
@Admin

Narrative:
As an Admin
I want to edit user's comment
So that I can provide appropriate content for users in the SAN

Scenario: Admin edit user's comment

Given Element logInLink is present
When Click logInLink element
And Type usernameTest in userNameField field
And Type passwordTest in passwordField field
And Click logInButton element
And Type adminWillUpdateThisComment in postCommentField field
And Click createCommentButton element
And Current user logs out
And Click logInLink element
And Type usernameAdmin in userNameField field
And Type passwordAdmin in passwordField field
And Click logInButton element
And Wait lastPostCreated to present for 10 seconds
And Click commentEditButton element
And Type adminChangesThisComment in postEditTextField field
And Click commentEditButton element
Then Verify Admin changed the content of this comment is equal to updatedCommentText
And Current user logs out