Meta:
@Admin

Narrative:
As an Admin
I want to delete user's post
So that I can protect the SAN from unappropriate content

Scenario: Admin delete user's post

Given Element logInLink is present
When Click logInLink element
And Type usernameTest in userNameField field
And Type passwordTest in passwordField field
And Click logInButton element
And Type adminWillDeleteThisPost in postTextField field
And Click setPublicVisibility element
And Click createNewPostButton element
And Current user logs out
And Click logInLink element
And Type usernameAdmin in userNameField field
And Type passwordAdmin in passwordField field
And Click logInButton element
And Wait lastPostCreated to present for 10 seconds
When Click postDeleteButton element
Then Element adminWillDeleteThisPost is not exists in lastPostCreated
And Current user logs out