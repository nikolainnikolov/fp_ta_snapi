Meta:
@Login

Narrative:
As a registered user
I want to log in to my account

Scenario: Log in successfully as existing user
Given Element logInLink is present
When Click logInLink element
Then Type username in userNameField field
Then Type password in passwordField field
Then Click logInButton element
Then Element userProfileLink is present