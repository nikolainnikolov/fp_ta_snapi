Meta:
@UserProfile

Narrative:
As a user
I want to register successfully and update my profile information
So that I can connect with other SAN users and share more personal information with them

Scenario: User registers in SAN, logs in and updates a profile information
Given Element homepage is present
When Click registerLink element
And Element registerUsernameField is present
And Type usernameNewUser in registerUsernameField field
And Type emailNewUser in registerEmailField field
And Type passwordNewUser in registerPasswordField field
And Type confirmPasswordNewUser in registerConfirmPasswordField field
And Type firstNameNewUser in registerFirstNameField field
And Type lastNameNewUser in registerLastNameField field
And Type dateNewUser in registerBirthDateField field
And Type addressNewUser in registerAddressField field
And Type cityOfBirthNewUser in registerCityOfBirthField field
And Type planetOfBirthNewUser in registerPlanetField field
And Type cityOfResidenceNewUser in registerCityOfResidenceField field
And Type planetOfResidenceNewUser in registerPlanetOfResidenceField field
And Type jobNewUser in registerJobTitleField field
And Type educationNewUser in registerEducationLevelField field
And Click registerButton element
Then Element successfulRegistrationMessage is present

Given Element successfulRegistrationMessage is present
When Click logInLink element
And Type usernameNewUser in userNameField field
And Type passwordNewUser in passwordField field
And Click logInButton element
Then Element userProfileLink is present

Given Element userProfileLink is present
When Click userProfileLink element
Then Element userProfilePicture is present
And Element First name: T1 is equal to userFirstNameInUserProfilePanel element
And Element Last name: K1 is equal to userLastNameInUserProfilePanel element
And Element Username: R1 is equal to usernameInUserProfilePanel element
And Element Email: r1@mail.bg is equal to userEmailInUserProfilePanel element
And Element Education level: secret is equal to userEducationINProfilePanel element
And Element Job title: doctor is equal to userJobINProfilePanel element
And Element City of Residence: Crazy Town is equal to userCityOfResidenceINProfilePanel element
And Element City of Birth: Crazy Town is equal to userCityOfBirthINProfilePanel element
And Element Address: Current str is equal to userAddressINProfilePanel element

Given Element userProfileLink is present
And Click userProfileLink element
And Wait userProfileFirstName to present for 3 seconds
And Type updatedFirstName in userProfileFirstName field
And Type updatedLastName in userProfileLastName field
And Type updatedEmail in userProfileEmail field
And Type updatedAddress in userProfileAddress field
And Type updatedDate in userProfileBirthDate field
And Type updatedCityOfBirth in userProfileCityOfBirth field
And Type updatedPlanetOfBirth in userProfilePlanetOfCityOfBirth field
And Type updatedCityOfResidence in userProfileCityOfResidence field
And Type updatedPlanetOfResidence in userProfilePlanetOfCityOfResidence field
And Type updatedJob in userProfileJobTitle field
And Type updatedEducation in userProfileEducationLevel field
And Click userProfileUpdateInfoButton element
Then Element successfulUpdateProfileMessage is present

Given Element userProfileLink is present
When Click userProfileLink element
Then Verify Username: R1 is equal to usernameInUserProfilePanel
And Verify First name: Tuturutka is equal to userFirstNameInUserProfilePanel
And Verify Last name: No1 is equal to userLastNameInUserProfilePanel
And Verify Email: tuturutkano1@mail.bg is equal to userEmailInUserProfilePanel
And Verify Education level: High Level is equal to userEducationINProfilePanel
And Verify Job title: doctor-rest is equal to userJobINProfilePanel
And Verify City of Residence: Crazy City is equal to userCityOfResidenceINProfilePanel
And Verify City of Birth: Crazy City is equal to userCityOfBirthINProfilePanel
And Verify Address: MyCurrent str is equal to userAddressINProfilePanel
And Current user logs out
