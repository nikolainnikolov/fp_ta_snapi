
(stories/TC_003_CreateTopicTitle4CharsLength.story)
Meta:
@topic 

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal
Scenario: User try to create topic with Title length less than minimal alert
Given Element createTopicButton is present
When Click createTopicButton element (FAILED)
(org.openqa.selenium.ElementClickInterceptedException: Element <span class="d-button-label"> is not clickable at point (1452,207) because another element <div class="modal-backdrop in"> obscures it
Build info: version: '3.14.0', revision: 'aacccce0', time: '2018-08-02T20:19:58.91Z'
System info: host: 'VESELA-PC', ip: '192.168.0.192', os.name: 'Windows 10', os.arch: 'x86', os.version: '10.0', java.version: '1.8.0_221'
Driver info: org.openqa.selenium.firefox.FirefoxDriver
Capabilities {acceptInsecureCerts: true, browserName: firefox, browserVersion: 69.0.3, javascriptEnabled: true, moz:accessibilityChecks: false, moz:buildID: 20191009172106, moz:geckodriverVersion: 0.26.0, moz:headless: false, moz:processID: 12492, moz:profile: C:\Users\Vesela\AppData\Loc..., moz:shutdownTimeout: 60000, moz:useNonSpecCompliantPointerOrigin: false, moz:webdriverClick: true, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, platformVersion: 10.0, rotatable: false, setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify}
Session ID: 2823adde-f6f4-468e-b9e1-60b593f3971a)
And Type topicTitle4chars in createTopicTitleField field (NOT PERFORMED)
And Type textBaseLength in createTopicTextArea field (NOT PERFORMED)
And Click createNewTopicButton element (NOT PERFORMED)
Then Element topicTitleLengthOutOfBoundaryAlert is present (NOT PERFORMED)


