Meta:
@topic

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: User create a post with link as title
Given Element homepage is present
When Type http://google.com in postTextField field
And Click setPublicVisibility element
And Click createNewPostButton element
And Click homepage element
Then Element NewPostVisibility is present
