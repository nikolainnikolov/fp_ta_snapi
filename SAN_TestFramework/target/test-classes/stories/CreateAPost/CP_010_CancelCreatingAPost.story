Meta:
@topictest

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: User open new post popup, enter text and camcel it
Given Element homepage is present
When Click createPostButton element
And Enter text with 5 symbols in postTextField field
And Click cancelNewPostButton element
Then Element userFeedPage is present
And Element NewPostVisibility is not present