Meta:
@topic
Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: User try to create topic with Title length less than minimal alert
Given Element createTopicButton is present
When Click createTopicButton element
And Type topicTitle4chars in createTopicTitleField field
And Type textBaseLength in createTopicTextArea field
And Click createNewTopicButton element
Then Element topicTitleLengthOutOfBoundaryAlert is present