Meta:
@UserProfile

Narrative:
As a user
I want to register successfully and update my profile information
So that I can connect with other SAN users and share more personal information with them

Scenario: User registers in SAN, logs in and updates a profile information
Given Element homepage is present
When Click registerLink element
And Element registerUsernameField is present
And Type usernameNewUser in registerUsernameField field
And Type emailNewUser in registerEmailField field
And Type passwordNewUser in registerPasswordField field
And Type confirmPasswordNewUser in registerConfirmPasswordField field
And Type firstNameNewUser in registerFirstNameField field
And Type lastNameNewUser in registerLastNameField field
And Type dateNewUser in registerBirthDateField field
And Type addressNewUser in registerAddressField field
And Type cityOfBirthNewUser in registerCityOfBirthField field
And Type planetOfBirthNewUser in registerPlanetField field
And Type cityOfResidenceNewUser in registerCityOfResidenceField field
And Type planetOfResidenceNewUser in registerPlanetField field
And Type jobNewUser in registerJobTitleField field
And Type educationNewUser in registerEducationLevelField field
And Click registerButton element
And Click Home element
And Click logInLink element
And Type usernameNewUser in userNameField field
And Type passwordNewUser in passwordField field
And Click logInButton element
And Element homepage is present
And Click userProfileLink element
And Element usernameNewUser is equal to userProfilePanel element
And Element userProfilePicture= is present
And Element emailNewUser is equal to userProfilePanel element
And Element firstNameNewUser is equal to userProfilePanel element
And Element lastNameNewUser is equal to userProfilePanel element
And Element dateNewUser is equal to userProfilePanel element
And Element addressNewUser  is equal to userProfilePanel element
And Element cityOfBirthNewUser   is equal to userProfilePanel element
And Element planetOfBirthNewUser   is equal to userProfilePanel element
And Element cityOfResidenceNewUser   is equal to userProfilePanel element
And Element planetOfResidenceNewUser  is equal to userProfilePanel element
And Element jobNewUser  is equal to userProfilePanel element
And Element educationNewUser  is equal to userProfilePanel element
And Click userProfileLink element
And Wait userProfileFirstName to present for 4 seconds
And Type updatedFirstName in userProfileFirstName field
And Type updatedLastName in userProfileLastName field
And Type updatedEmail in userProfileEmail field
And Type updatedAddress in userProfileAddress field
And Type updatedDate in userProfileBirthDate field
And Type updatedCityOfBirth in userProfileCityOfBirth field
And Type updatedPlanetOfBirth in userProfilePlanetOfCityOfBirth field
And Type updatedCityOfResidence in userProfileCityOfResidence field
And Type updatedPlanetOfResidence in userProfilePlanetOfCityOfResidence field
And Type updatedJob in userProfileJobTitle field
And Type updatedEducation in userProfileEducationLevel field
And Click userProfileUpdateInfoButton element
Then Element userProfilePanel is present
And Element updatedFirstName is equal to userProfilePanel element
And Element  updatedLastName is equal to userProfilePanel element
And Element  updatedEmail is equal to userProfilePanel element
And Element  updatedAddress is equal to userProfilePanel element
And Element  updatedDate  is equal to userProfilePanel element
And Element  updatedCityOfResidence is equal to userProfilePanel element
And Element  updatedPlanetOfResidence is equal to userProfilePanel element
And Element  updatedJob is equal to userProfilePanel element
And Element  updatedEducation is equal to userProfilePanel element
And Current user logs out
