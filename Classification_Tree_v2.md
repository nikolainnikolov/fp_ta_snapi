```mermaid
graph TB;
A[Social Network]-->B(Connect with people)
A-->C(Create, comment and like posts)
A-->D(Get feed of the newest/most relevant posts of users' connections)

E[UI]-->F(User-friendly)
E-->G(Simple)
E-->H(Intuitive)

```


```mermaid
graph TB;
A((Web App))-->B{Public part}
A-->C{Private part}
A-->D{Administration part}

```


```mermaid
graph TB;
B{Public part}---B0>visible without authentication]
B-->E[Landing page]
    E-->I(Links to login and registration)
    I-->I1(Search box)
    I1-->I2(Public feed)
    I2-.-J("(optional) Forgotten password, email confirmation")
B-->F[Public profiles]
    F-->K(Show info that the user decided to be public)
    K---K1("e.g name, picture, posts (chronologically)")
B-->G[Search]
    G---L(Based on name and/or email)
B-->H[Public feed]

```


```mermaid
graph TB;
C{Private part}---N>visible after successfull login]
C-->O[Profile administration page]
    O-->O1(change name)
    O1-->O2(upload a profile picture)
    O2-->O3("set visibility (public or private)")
    O3-.->O4("(optional) change password")
    O4-.-O5("(optional) change email")
    O5-.-O6("(optional) add age, nationality, job etc.")

C-->P[Request to connect or disconnect to another user, while viewing his profile]
    P---P1>connection request needs approval]
    P1---P2>disconnection doesn't need approval]

C-->Q[When creating a post]
    Q---Q1>must choose public or private]
    Q-->Q2(The post can contain text)
    Q2-.-Q3("(optional) Upload picture")
    Q3-.-Q4("(optional) Upload song")
    Q4-.-Q5("(optional) Upload video")
    Q5-.-Q6("(optional) Location")

```


```mermaid
graph TB;
C{Private part}-->R["Private feed from users connections (chronological)"]
    R-.-R1("(optional) sort posts by comments, likes etc.")
    R1-.-R2("(optional) sort based on location")
    R2-.-R3("(optional) use above algorith for the public feed")

C-->S[Like a post]
    S---S1>unlike, if previously liked]
    S1-->S2(Total like count, showed along the post content)
    S2-.-S3("(optional) include comment like count in feed generation algorithm")

C-->T[Comment a post]
    T---T1>under each post there is a comment section]
    T-->T2(The newest n comments are shown)
    T2---T3>n must be defined]
    T3---T4>expand section for older comments]
    T4-.-T5("(optional) comment reply function")

```


```mermaid
graph TB;
D{Administration part}-->U(Should be able to:)
    U---U1(edit/delete profiles)
    U---U2(edit/delete posts)
    U---U3(edit/delete comments)

```