## HIGH LEVEL TESTS

>Version 1.0, October 25, 2019

>Prepared  by: _Nikolay Nikolov_, _Borislav Georgiev_ and _Vesela Slavkova_

### USER PROFILE ADMINISTRATIVE PART

*Public part*

* Verify that registered users` name is visible on Application home page without authentication after successful registration.
* Verify that users can choose for their profile pictire to be public and the same picture is visible on Application home page without authentication.

*Private part*

* Verify that users can change their name in their pofile administrative page.
* Verify that users can change their profile pictire in their pofile administrative page.
* Verify that users can choose for their profile pictire to be pivate and it is visible only for their friends/connections in the Social Network.
* Verify that users can change their Email in their pofile administrative page.
* Verify that users can change their password in their pofile administrative page.
* Verify that users can change other personal information in their pofile administrative page.

*Negative + Error message*

* Verify that users cannot change their profile name with empty field and this try should lead to error message.
* Verify that users cannot change their profile name with other,that contain less symbols than the allowed minimum and this try should lead to error message. 
* Verify that users cannot change their profile name with other,that contain more symbols than the allowed maximum and this try should lead to error message. 
* Verify that users cannot change their profile mail with invalid email format and this try should lead to error message.
* Verify that users cannot change their password with other,that contain less symbols than the allowed minimum and this try should lead to error message. 
* Verify that users cannot change their password with other,that contain unacceptable symbols and this try should lead to error message. 

### ADMINISTRATION PART

* Verify that Administrator can edit other users name and profile picture.
* Verify that Administrator can delete other users profile.
* Verify that Administrator can edit other users post.
* Verify that Administrator can delete other users post.
* Verify that Administrator can edit other users comment.
* Verify that Administrator can delete other users comment.

### POSTS GENERAL /Creating a post

* Verify that registered users can create a post that content minimum allowed symbols, choose for it to be public and the same is visible for everyone, including non-registered users of the Social Network.
* Verify that registered users can create a post that content maximum allowed symbols, choose for it to be private and the same is visible only for their friends/connections in the Social Network.
* Verify that registered users can create a post with link as content, choose for it to be private and the same link gets displayed and can be open by their friends/connections in the Social Network.

*Optional*

* Verify that registered users can upload a picture as post content, choose for it to be private and the same picture is visible for their friends/ connections in the Social Network.
* Verify that registered users can upload a song as post content, choose for it to be private and the same song can be listen to by their friends/ connections in the Social Network.
* Verify that uregistered users can upload a video as post content, choose for it to be public and the same video is visible for everyone, including non-registered users of the Social Network.
* Verify that registered users can upload a location and type some text as post content, choose for it to be private and the same is visible for their friends/connections in the Social Network.
* Verify that UI element for edit posts is displayed under created by user post and he/she can edit this post.
* Verify that UI element for delete posts is displayed under created by user post and he/she can delete this post.
* Verify that creating a post with the same content for second should lead to confirm popup and after confirmation the same is visible for  their friends/connections.

   *Negative + Error msgs* 

* Verify that registered users cannot create an empty post and this try should lead to error message.
* Verify that registered users cannot create a post that content less symbols than the allowed minimum and this try should lead to error message. 
* Verify that registeredusers cannot create a post that content more symbols than the allowed maximum and this try should lead to error message. 
* Verify that registered users cannot create a post without choosing it is public or private and this try should lead to warning message.
* Verify that unregistered users cannot create a post and this try should lead to LogIn page.

*Negative + Missing option for this action*

* Verify that current user cannot edit other users post.
* Verify that current user annot delete other users post.

### POST INTERACTIONS /Comment a post; Like a post

* Verify that users can create a comment to a post created by their friends/connections, this comment is visible for all friends/connections of the author of the post on the bottom of the page and counter for comments is added 1 to its current value.
* Verify that registered users can create a comment to a public post created by other user who is not their friend/connection.
* Verify that users can like a post created by their friends/connections and counter for likes is added 1 to its current value.
* Verify that registered users can like a public post created by other user who is not their friend/connection.
* Verify that users can dislikes a post they have already liked.
* Verify that users can see the newest n (10) comments and expand the area allows visibility of all comments to this post.

   *Negative + Error msgs*

* Verify that users cannot dislike a post they have not previously liked and this try should lead to error message.
* Verify that registered users cannot like the same post twice and this try should lead to error message.
* Verify that unregistered users cannot create a comment to a post and this try should lead to LogIn page.
* Verify that unregistered users cannot like a post and this try should lead to LogIn page.
* Verify that unregistered users cannot like a comment and this try should lead to LogIn page.

*Negative + Missing option for this action*

* Verify that current user cannot edit other users comment.
* Verify that current user cannot delete other users comment.

*Optional*
* Verify that registered users can like a comment to a post created by other user and counter for comment likes is added 1 to its current value.

### POST FEED VISIBILITY
*Public part*

* Verify that created as public posts are visible on Application home page without authentication in chronologically order.
* Verify that unregistered users can open a public post and all the content of the post gets dispalyed.
* Verify that created as private posts are not visible on Application home page without authentication. 

*Private part*

* Verify that users can see their custom post feed  in chronologically order.
* Verify that users can change the order of visibility of their custom post feed based on number of comments for a post.
* Verify that users can change the order of visibility of their custom post feed based on number of likes for a post.
* Verify that users can change the order of visibility of their custom post feed based on user location.