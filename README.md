# FP_TA_SNAPI

This repo is created for the final project (FP) at Telerik Academy (TA). To be specific a full cycle of QA activites will be developed for a Social Network API (SNAPI)

## Developer Repo - [Social Alien Network](https://gitlab.com/martikaragy/socialalien)

## QA Trello board - [BnV-QA-Team](https://trello.com/b/J2ecUCIW/bnv-qa-team)

## Test Cases - [High Level](https://docs.google.com/spreadsheets/d/1Z4bhQUM04WdaqvCKAkIk2F9r4dAznnD-p0Qj5a9y4_g/edit?ts=5db9a756#gid=2138675703&fvid=1988656727)

## Exploratory Test - [Charter](https://docs.google.com/spreadsheets/d/1oaFFmfxQvPoJnkGiM0Gw2vWgfd1XyR427sjTRi_k5sc/edit#gid=873913488)

## Exploratory Test - [List](https://docs.google.com/spreadsheets/d/1w6cxq59g86MLlQet69bb9of4WOqSve8XzfKo4egy5hw/edit#gid=632392335)

## Registered Issues - [In Dev Repo](https://gitlab.com/martikaragy/socialalien/issues)

## [Bug List](https://docs.google.com/spreadsheets/d/1CidBxIm58D9bXARel6O-_oyehIqmFk5vjdFJNiubhsc/edit?ts=5de41501#gid=0)

## Bug Report - [Bug Report](https://documentcloud.adobe.com/link/track?uri=urn%3Aaaid%3Ascds%3AUS%3Addc35c7d-04e0-427c-a22f-f54a67f51f36)

## Test Report - [Test Report](https://documentcloud.adobe.com/link/track?uri=urn%3Aaaid%3Ascds%3AUS%3A677b8c86-45fc-4484-9d23-0da32bea1406)

