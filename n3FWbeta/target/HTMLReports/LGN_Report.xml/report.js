$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/SAfeatureFiles/Login.feature");
formatter.feature({
  "line": 1,
  "name": "Login functionality of Social Alien(SA) social network web-application",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 5,
  "name": "Login in SA with valid username and password",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-valid-username-and-password",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Registered user launches the SA LoginPage with URL \"\u003curl\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "The user enters valid userName as \"\u003cuserName\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "The user enters valid password as \"\u003cpassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The user clicks on the login form submit button",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "The user is successfully logged in SA",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "User`s name is presented as a link at the navigation",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "the user closes the browser",
  "keyword": "Then "
});
formatter.examples({
  "line": 15,
  "name": "",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-valid-username-and-password;",
  "rows": [
    {
      "cells": [
        "url",
        "userName",
        "password"
      ],
      "line": 16,
      "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-valid-username-and-password;;1"
    },
    {
      "cells": [
        "http://localhost:8080/login",
        "N3TestUser",
        "123"
      ],
      "line": 17,
      "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-valid-username-and-password;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 17,
  "name": "Login in SA with valid username and password",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-valid-username-and-password;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Registered user launches the SA LoginPage with URL \"http://localhost:8080/login\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "The user enters valid userName as \"N3TestUser\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "The user enters valid password as \"123\"",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The user clicks on the login form submit button",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "The user is successfully logged in SA",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "User`s name is presented as a link at the navigation",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "the user closes the browser",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://localhost:8080/login",
      "offset": 52
    }
  ],
  "location": "LGNstepDefinitions.registeredUserLaunchesTheSALoginPageWithURL(String)"
});
formatter.result({
  "duration": 10299486900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "N3TestUser",
      "offset": 35
    }
  ],
  "location": "LGNstepDefinitions.theUserEntersValidUserNameAs(String)"
});
formatter.result({
  "duration": 57871100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123",
      "offset": 35
    }
  ],
  "location": "LGNstepDefinitions.theUserEntersValidPasswordAs(String)"
});
formatter.result({
  "duration": 47922000,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserClicksOnTheLoginFormSubmitButton()"
});
formatter.result({
  "duration": 232127400,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserIsSuccessfullyLoggedInSA()"
});
formatter.result({
  "duration": 76332900,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.userSNameIsPresentedAsALinkAtTheNavigation()"
});
formatter.result({
  "duration": 22788700,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserClosesTheBrowser()"
});
formatter.result({
  "duration": 622698100,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 22,
  "name": "Login/Logout with invalid user and valid password",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login/logout-with-invalid-user-and-valid-password",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 21,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "The guest user launches Social Alien (SA) home page with  URL \"\u003curl\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 25,
  "name": "The user goes to Login page",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "The user enters valid userName as \"\u003cuserName\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "The user enters valid password as \"\u003cpassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "The user clicks on the login form submit button",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "The user is successfully logged in SA",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "User`s name is presented as a link at the navigation",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "The user  clicks on logout button",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "The user is successfully logged-out from SA",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "The user is on the home page",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "the user closes the browser",
  "keyword": "Then "
});
formatter.examples({
  "line": 36,
  "name": "",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login/logout-with-invalid-user-and-valid-password;",
  "rows": [
    {
      "cells": [
        "url",
        "userName",
        "password"
      ],
      "line": 37,
      "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login/logout-with-invalid-user-and-valid-password;;1"
    },
    {
      "cells": [
        "http://localhost:8080",
        "NovemberRain",
        "novemberrain"
      ],
      "line": 38,
      "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login/logout-with-invalid-user-and-valid-password;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 38,
  "name": "Login/Logout with invalid user and valid password",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login/logout-with-invalid-user-and-valid-password;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 21,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "The guest user launches Social Alien (SA) home page with  URL \"http://localhost:8080\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 25,
  "name": "The user goes to Login page",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "The user enters valid userName as \"NovemberRain\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "The user enters valid password as \"novemberrain\"",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "The user clicks on the login form submit button",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "The user is successfully logged in SA",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "User`s name is presented as a link at the navigation",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "The user  clicks on logout button",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "The user is successfully logged-out from SA",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "The user is on the home page",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "the user closes the browser",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://localhost:8080",
      "offset": 63
    }
  ],
  "location": "LGNstepDefinitions.theGuestUserLaunchesSocialAlienSAHomePageWithURL(String)"
});
formatter.result({
  "duration": 9607770800,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserGoesToLoginPage()"
});
formatter.result({
  "duration": 255037900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NovemberRain",
      "offset": 35
    }
  ],
  "location": "LGNstepDefinitions.theUserEntersValidUserNameAs(String)"
});
formatter.result({
  "duration": 54415700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "novemberrain",
      "offset": 35
    }
  ],
  "location": "LGNstepDefinitions.theUserEntersValidPasswordAs(String)"
});
formatter.result({
  "duration": 60888800,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserClicksOnTheLoginFormSubmitButton()"
});
formatter.result({
  "duration": 245973100,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserIsSuccessfullyLoggedInSA()"
});
formatter.result({
  "duration": 95132200,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.userSNameIsPresentedAsALinkAtTheNavigation()"
});
formatter.result({
  "duration": 85054800,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.the_user_clicks_on_logout_button()"
});
formatter.result({
  "duration": 439197000,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.the_user_is_successfully_logged_out_from_SA()"
});
formatter.result({
  "duration": 26737500,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserIsOnTheHomePage()"
});
formatter.result({
  "duration": 6052900,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserClosesTheBrowser()"
});
formatter.result({
  "duration": 700866000,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 41,
  "name": "Login in SA with invalid username and valid password",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-invalid-username-and-valid-password",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 40,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 43,
  "name": "Registered user launches the SA LoginPage with URL \"\u003curl\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "The user enters invalid userName as \"\u003cinvalidUser\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "The user enters valid password as \"\u003cpassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "The user clicks on the login form submit button",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "The user is not logged in SA",
  "keyword": "Then "
});
formatter.step({
  "line": 48,
  "name": "The user stays on the loginPage",
  "keyword": "Then "
});
formatter.step({
  "line": 49,
  "name": "A error message is shown",
  "keyword": "Then "
});
formatter.step({
  "line": 50,
  "name": "the user closes the browser",
  "keyword": "Then "
});
formatter.examples({
  "line": 52,
  "name": "",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-invalid-username-and-valid-password;",
  "rows": [
    {
      "cells": [
        "url",
        "invalidUser",
        "password"
      ],
      "line": 53,
      "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-invalid-username-and-valid-password;;1"
    },
    {
      "cells": [
        "http://localhost:8080/login",
        "N3TestUser",
        "123"
      ],
      "line": 54,
      "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-invalid-username-and-valid-password;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 54,
  "name": "Login in SA with invalid username and valid password",
  "description": "",
  "id": "login-functionality-of-social-alien(sa)-social-network-web-application;login-in-sa-with-invalid-username-and-valid-password;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 40,
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "line": 43,
  "name": "Registered user launches the SA LoginPage with URL \"http://localhost:8080/login\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "The user enters invalid userName as \"N3TestUser\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "The user enters valid password as \"123\"",
  "matchedColumns": [
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "The user clicks on the login form submit button",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "The user is not logged in SA",
  "keyword": "Then "
});
formatter.step({
  "line": 48,
  "name": "The user stays on the loginPage",
  "keyword": "Then "
});
formatter.step({
  "line": 49,
  "name": "A error message is shown",
  "keyword": "Then "
});
formatter.step({
  "line": 50,
  "name": "the user closes the browser",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://localhost:8080/login",
      "offset": 52
    }
  ],
  "location": "LGNstepDefinitions.registeredUserLaunchesTheSALoginPageWithURL(String)"
});
formatter.result({
  "duration": 9694850300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "N3TestUser",
      "offset": 37
    }
  ],
  "location": "LGNstepDefinitions.theUserEntersInvalidUserNameAs(String)"
});
formatter.result({
  "duration": 53869800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123",
      "offset": 35
    }
  ],
  "location": "LGNstepDefinitions.theUserEntersValidPasswordAs(String)"
});
formatter.result({
  "duration": 61869500,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserClicksOnTheLoginFormSubmitButton()"
});
formatter.result({
  "duration": 228739700,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserIsNotLoggedInSA()"
});
formatter.result({
  "duration": 23455200,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserStaysOnTheLoginPage()"
});
formatter.result({
  "duration": 5559700,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.aErrorMessageIsShown()"
});
formatter.result({
  "duration": 22606000,
  "status": "passed"
});
formatter.match({
  "location": "LGNstepDefinitions.theUserClosesTheBrowser()"
});
formatter.result({
  "duration": 632117900,
  "status": "passed"
});
});