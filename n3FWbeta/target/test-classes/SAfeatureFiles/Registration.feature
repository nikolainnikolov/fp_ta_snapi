Feature: Registration functionality of Social Alien(SA) social network web-application
  As a  registered user,
  I want to be able to login in the SA
  so that I can use all user`s functionality of the SA App

  @Smoke
  Scenario Outline: Registration with must field fields

    Given A guest user lunches the SA registration page with with URL "<url>"
    When The user enters new userName as "<userName>"
    When The user enters new email as "<email>"
    When The user enters password as "<password>"
    When The user reenters the password
    And The user clicks on the registration form submit button
    Then The user is successfully registered in SA
    Then A welcome message is shown at the home page
    Then the user closes the browser

    Examples:
      | url| userName|email|password|
      |http://localhost:8080/register | newU|email | newPass|
