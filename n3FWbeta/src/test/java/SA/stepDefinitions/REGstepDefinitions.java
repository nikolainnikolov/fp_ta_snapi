package SA.stepDefinitions;


import SA.POM.HomePageClass;
import SA.POM.LoginPageClass;
import SA.POM.RegistrationPageClass;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import n3qa.WD.support.DriverSvS;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static org.testng.Assert.assertEquals;

public class REGstepDefinitions {
    private WebDriver driver;
    private WebElement element;
    private WebDriverWait wait;
    private DriverSvS services;
    private n3qa.WD.settings.SA SA;

    public  String regPassword ;
    public  String confPass ;

    public REGstepDefinitions(DriverSvS services, n3qa.WD.settings.SA SA) {
        this.services = services;
        this.driver = services.getDriver();
        this.SA = SA;
    }


    @Given("^A guest user lunches the SA registration page with with URL \"([^\"]*)\"$")
    public void a_guest_user_lunches_the_sa_registration_page_with_with_url_something(String url)  {
        driver.get(url);
        String URL = driver.getCurrentUrl();
        Assert.assertEquals(URL, "http://localhost:8080/register" );

        SA.registrationPage = new RegistrationPageClass(driver);
    }

    @When("^The user enters new userName as \"([^\"]*)\"$")
    public void the_user_enters_new_username_as_something(String username) throws Throwable {
        String ranGen = RandomStringUtils.randomAlphanumeric(3);
        System.out.println(ranGen);
        SA.registrationPage.REG_UsernameFIELD.sendKeys(username+ranGen);
    }

    @When("^The user enters new email as \"([^\"]*)\"$")
    public void the_user_enters_new_email_as_something(String email) throws Throwable {
        String ranGen = RandomStringUtils.randomAlphanumeric(3);
        System.out.println(ranGen);

        SA.registrationPage.REG_EmailFIELD.sendKeys(email+"@"+ranGen+".com");
    }

    @When("^The user enters password as \"([^\"]*)\"$")
    public void the_user_enters_new_password_as_something(String password)   {

        String ranGen = RandomStringUtils.randomAlphanumeric(3);
        regPassword = password+ranGen+ranGen;
        System.out.println("The user`s registration password is: "+regPassword);
        SA.registrationPage.REG_PasswordFIELD.sendKeys(regPassword);
        confPass = regPassword;
    }


    @When("^The user reenters the password$")
    public void theUserReentersThePasswordAs() {

        //Нарочно е направено така за да покаже грешка
        System.out.println("The user`s confirm the password with: "+confPass);
        SA.registrationPage.REG_ConfirmPassword.sendKeys(confPass);
    }

    @And("^The user clicks on the registration form submit button$")
    public void the_user_clicks_on_the_registration_form_submit_button() throws Throwable {
        SA.registrationPage.REG_FormSubmitBTN.click();
    }

    @Then("^The user is successfully registered in SA$")
    public void the_user_is_successfully_registered_in_sa()   {
        System.out.println("OK");
    }

    @Then("^A welcome message is shown at the home page$")
    public void a_welcome_message_is_shown_at_the_home_page() throws Throwable {
        System.out.println("Super");
    }

}
